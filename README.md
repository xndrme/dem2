# DEM2 # {#mainpage}

### ¿Qué es DEM2? ###

Es una plataforma de simulación para el Método de Elementos Discretos modular, altamente expandible y con varias
implementaciones de cada uno de sus componentes, lo que la hace ideal para la experimentación académica.
La versión acntual es una version de prueba: 0.1

Dependencias:
--------------

*  Boost library = 1.58
*  MGCD Lib >= 1.0
*  VTK = 6.2
*  Gtest >= 1.6 (Solo para las pruebas)

Dependencias para compilación:
------------------------------

* CMake >= 3.2

### ¿Cómo utilizar? ###

DEM2 compila una biblioteca estática. Para us compilación primero hay que compilar las bibliotecas Boost, MGCD y VTK
como bibliotecas estáticas y luego compilar el código DEM2. Puede utilizarse la biblioteca para linkear con un proyecto
externo, o utilizar directamente el ejecutable que se genera.

#### DEM2 compila: ####

 * una biblioteca que se puede utilizar en cualquier proyecto externo, un ejecutable
 * un ejecutable que permite correr simulaciones de la siguiente forma: <code>DEM2 input_file.csv</code>
 * un ejecutable con las pruebas unitarias

### Contacto ###

* Admin: xndrme@ya.ru
* carecarey@gmail.com