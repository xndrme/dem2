# minR: 1.000070
# maxR: 2.000000
# xMin: -10.759051
# yMin: -10.601462
# zMin: -1.045379
# xMax: 10.830410
# yMax: 10.732830
# zMax: 20.356191
# coulomb: 0.2
# step: 0.0001
# iters: 100000
# gravAcc: 9.80665
# knc: 1000000
# knt: 1000000
# integrator: gear
# rt: 10000000
# rn: 10000000
# kt: 900000000
# cohesivedistance: 0.0655597276608
# coll: mgcd,4.0001,n
# record: txt,test_output/dump_TriangleSphere.txt,500
[sphere]
center_x,center_y,center_z,radius,mass,inertiaMoment_xx,inertiaMoment_xy,inertiaMoment_xz,inertiaMoment_yx,inertiaMoment_yy,inertiaMoment_yz,inertiaMoment_zx,inertiaMoment_zy,inertiaMoment_zz,mobile,fixedPoint_x,fixedPoint_y,fixedPoint_z
0,0,10,2,8,8,0,0,0,8,0,0,0,8,1,0,0,12
[triangle]
center_x,center_y,center_z,v0_x,v0_y,v0_z,v1_x,v1_y,v1_z,v2_x,v2_y,v2_z,mobile
0,0,0,1,0,0,-0.8,0.6,0,-0.8,-0.6,0,0


