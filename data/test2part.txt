[global]
# minR: 2
# maxR: 5
# xMin: -10
# yMin: -10
# zMin: -10
# xMax: 10
# yMax: 10
# zMax: 20
# coulumb: 0.2
# step: 0.001
# iters: 10000
# gravAcc: 9.80665
# knc: 1000000
# knt: 1000000
# integrator: gear

[sphere]
<center, radius, idx, vel, angVel, acc, angAcc, F, mass, inertiaMoment, mobile, centerBefore, fixedPoint, rotAng>
{{0, 0, 10}, {2}, {0}, {0, 10, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {5}, {8,0,0,0,8,0,0,0,8}, {1}, {0, 0, 10}, {0, 0, 12}, {0, 0, 0}}
{{0, 10, 0}, {2}, {1}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {5}, {8,0,0,0,8,0,0,0,8}, {0}, {0, 10, 0}, {0, 10, 2}, {0, 0, 0}}
