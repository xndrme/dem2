# minR: 1.000070
# maxR: 2.000000
# xMin: -10.759051
# yMin: -10.601462
# zMin: -1.045379
# xMax: 10.830410
# yMax: 10.732830
# zMax: 20.356191
# step: 0.0001
# iters: 100000
# gravAcc: 0.0
# integrator: gear
# cohesivedistance: 0.0655597276608
# coll: mgcd,4.0001,n
# models: default,wallCohesive
# record: txt,test_output/dump_PartsConcepts.txt,500
[material]
model,knc,knt,kt,rn,rt,coulomb
0,10000000,10000000,10000000,100,100,0.2
1,11000000,0,0,0,0,0
[interface]
materialId1,materialId1,model,rn,rt
0,1,1,0,0
[sphere]
center_x,center_y,center_z,radius,mass,inertiaMoment_xx,inertiaMoment_xy,inertiaMoment_xz,inertiaMoment_yx,inertiaMoment_yy,inertiaMoment_yz,inertiaMoment_zx,inertiaMoment_zy,inertiaMoment_zz,mobile,fixedPoint_x,fixedPoint_y,fixedPoint_z,velocity_x,velocity_y,velocity_z,material
3.5,-4.04145188432738,0,2,8,8,0,0,0,8,0,0,0,8,1,3.5,-4.04145188432738,2,-3.5,4.04145188432738,0,0
3.5,4.04145188432738,0,2,8,8,0,0,0,8,0,0,0,8,1,3.5,4.04145188432738,2,-3.5,-4.04145188432738,0,0
-7,0,0,2,8,8,0,0,0,8,0,0,0,8,1,7,0,2,7,0,0,0
-14,-5,0,2,8,8,0,0,0,8,0,0,0,8,1,-14,-5,2,0,5,0,0
-14,5,0,2,8,8,0,0,0,8,0,0,0,8,1,-14,5,2,0,-5,0,0
0,-8,-6,2,8,8,0,0,0,8,0,0,0,8,1,0,-8,0,0,0,0,0
[triangle]
center_x,center_y,center_z,v0_x,v0_y,v0_z,v1_x,v1_y,v1_z,v2_x,v2_y,v2_z,mobile,material
0,-8,-4.01,-1,0,0,1,-1,0,1,1,0,0,1
