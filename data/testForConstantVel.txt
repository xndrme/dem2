# minR: 1.000070
# maxR: 2.000000
# xMin: -10.759051
# yMin: -10.601462
# zMin: -1.045379
# xMax: 10.830410
# yMax: 10.732830
# zMax: 20.356191
# coulomb: 0.2
# step: 0.0000001
# iters: 1000000
# gravAcc: 0.0
# knc: 1000000000
# knt: 1000000000
# integrator: gear
# rt: 10000000
# rn: 10000000
# kt: 900000000
# cohesivedistance: 0.0655597276608
# coll: mgcd,4.0001,n
# record: txt,test_output/dump_testForConstantVel.txt, 500
[sphere]
center_x,center_y,center_z,radius,mass,inertiaMoment_xx,inertiaMoment_xy,inertiaMoment_xz,inertiaMoment_yx,inertiaMoment_yy,inertiaMoment_yz,inertiaMoment_zx,inertiaMoment_zy,inertiaMoment_zz,mobile,fixedPoint_x,fixedPoint_y,fixedPoint_z
2.526255,0.362120,-0.043053,2.000000,8.000000,8.000000,0.000000,0.000000,0.000000,8.000000,0.000000,0.000000,0.000000,8.000000,0,2.526255,0.362120,1.956947
-0.252803,3.606401,-0.093107,2.000000,8.000000,8.000000,0.000000,0.000000,0.000000,8.000000,0.000000,0.000000,0.000000,8.000000,1,-0.252803,3.606401,1.906893
