//
// Created by Alvaro Javier on 7/1/2015.
//

#ifndef DEM2_PROGRESSREPORTER_H
#define DEM2_PROGRESSREPORTER_H


#include <core/GeneralPurposeEngine.h>

/**
 * @brief Muestra el progreso de la simulación en forma de porcentaje.
 */
class ProgressReporter : public GeneralPurposeEngine {
public:
    void beforeStep(Simulation* sim);
private:
    ///cantidada de iteraciones que repesentan el 1% del total de iteraciones de la simualción
    unsigned int iters;
    ///tiempo de inicio de la simualción
    int startTime;
    ///tiempo de finalizacion de la simulación
    int endTime;
};


#endif //DEM2_PROGRESSREPORTER_H
