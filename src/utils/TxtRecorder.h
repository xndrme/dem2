/*
 * File:   TxtRecorder.h
 * Author: xndrme
 *
 */

#ifndef TXTRECORDER_H
#define	TXTRECORDER_H

#include <core/GeneralPurposeEngine.h>

/**
 * @brief Guarda el estado de la simulación cada cierto número de pasos en un fichero de texto.
 */
class TxtRecorder : public GeneralPurposeEngine {

	std::string file,file_triangles;
	int iter;

public:
	TxtRecorder(const std::string& fileName, int iterStep);
	virtual void beforeStep(Simulation* sim);
	virtual ~TxtRecorder();

};

#endif	/* TXTRECORDER_H */

