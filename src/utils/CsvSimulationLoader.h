/*
 * CsvSimulationLoader.h
 *
 *  Created on: ene 18, 2015
 *      Author: xndrme
 */

#ifndef CSVSIMULATIONLOADER_H_
#define CSVSIMULATIONLOADER_H_

#include <map>
#include "boost/shared_ptr.hpp"

#include <core/Simulation.h>

#define INTERACTION_TYPE -1

#define MATERIAL_TYPE -2
#define INTERFACE_TYPE -3


#define SPHERE_TYPE 1
#define TRIANGLE_TYPE 3
#define PLANE_TYPE 2

using namespace std;

/**
 * @brief Esta clase es un cargador para simulaciones que lee un fichero csv con la información de la simulación
 *
 * @details ## Formato para el fichero de entrada ##
 *  <BLOCKQUOTE>
 *     \# \<prop\>: val[,val]*\n
 *     ...\n
 *     [section]\n
 *     col[,col]*\n
 *     val[,val]*\n
 *     [section]\n
 *     ...
 *  </BLOCKQUOTE>
 *
 * ### Detalles ###
 *     * <BLOCKQUOTE>
 *     \# \<prop\>: val[,val]*\n
 *     </BLOCKQUOTE>
 *     indica una propiedad de la simulación inicializada con los valores que la siguen, por ejemplo:\n
 *     \# coll : mgcd,3,5,an\n
 *     define que el detecto de colisiones de la simulacion será MGCD y se va a usar con 2 grillas, una con celdas de
 *     tamaño 3 y otra de tamaño 5, la última cadena de texto define que para las grillas se usará un algoritmo automá-
 *     tico primero y NBS para la segunda grilla.
 *
 *     * <BLOCKQUOTE>
 *     [section]
 *     </BLOCKQUOTE>
 *     dividen el fichero en secciones con los datos de entrada, por ejemplo:\n
 *     [sphere]\n
 *     define que lo que viene a continuación son las propiedades de un conjunto de esferas, y\n
 *     [interactions]\n
 *     define que lo que viene a continuación son las propiedades de las interacciones iniciales
 *
 *     * <BLOCKQUOTE>
 *     col[,col]*
 *     </BLOCKQUOTE>
 *     definen los nombres de las columnas para los datos que siguen, así se pueden incializar los objetos representados
 *     en el fichero, ejemplo:\n
 *     center_x,center_y,center_z,radius,...\n
 *     1,2,3,10,...\n *
 *     define una partícula con centro en (1,2,3) y radio 10 con otras propiedades a continuación. En el fichero de entrada
 *     pueden haber varias secciones, tantas como sean necesarias.
 *
 * # Especificación de cada propiedad #
 * A continuación se presenta una especificación completa para el fichero de entrada:
 *
 *  ## Propiedades globales de la simulación ##
 *  * Radios
 *     - <b>\# minR: double</b> radio mínimo de las partículas
 *     - <b>\# maxR: double</b> radio máximo de las partículas
 *  * Caja acotadora de la simulación
 *     - <b>\# xMin: double</b> valor mínimo de para la coordenada X
 *     - <b>\# yMin: double</b> valor mínimo de para la coordenada Y
 *     - <b>\# zMin: double</b> valor mínimo de para la coordenada Z
 *     - <b>\# xMax: double</b> valor máximo de para la coordenada X
 *     - <b>\# yMax: double</b> valor máximo de para la coordenada Y
 *     - <b>\# zMax: double</b> valor máximo de para la coordenada Z
 *  * Valores relativos al tiempo de ejecución
 *     - <b>\# step: double</b> paso de tiempo
 *     - <b>\# iters: integer</b> cantidad de iteraciones
 *  * Configuración de los componentes de un paso de la simulación
 *     - <b>\# integrator: name</b> define el integrador, <b>name</b> puede ser <b>gear</b> o <b>houbolt</b>.
 *     - <b>\# coll: name,double[,double]*,string</b> define el detector de colisiones que se utilizará, <b>name</b>
 *     por ahora solo puede ser <b>mgcd</b>, a continuación una lista de valores que representan los radios de las
 *     grillas, finalmente una cadena que define los algoritmos a utilizar en cada grilla A para automático, N para
 *     NBS y S para screening. En caso de utlizar otro detector de colisiones la configuración puede ser diferente.
 *     - <b>\# models: name[,name]</b> Lista de modelos a utlizar, <b>name</b> puede ser <b>default</b>,<b>wall</b>,
 *     <b>wallcohesive</b>,<b>cohesivefrictionalovertime</b>,<b>irvindamping</b>,<b>hertz</b>. Los IDs de los modelas
 *     se asignan según el orden en que se especifican los materiales empezando por 0 e icrementando en 1 para cada
 *     modelo.
 *  * Otras propiedades
 *     - <b>\# cohesivedistance: double </b> distancia máxima de cohesión
 *  * Fuerzas
 *     - <b>\# gravAcc: double</b> define una fuerza de gravedad global
 *     - <b>\# force: name,double,double,double[,double,double,double]?[,integer]*</b> aquí se define una fuerza que actúa
 *     en la simulación, las fuerzas pueden ser <b>constant</b>, <b>sin</b>, <b>cos</b>, que representan una fuerza
 *     constante, sinusuoidal o cosinusoidal. Para la fuerza constante se debe especificar un vector de fuerza, para
 *     las fuerzas sinu o cosinusuoidales dos vectores que representan los coeficientes a utlizar en el calculo de las
 *     fuerzas (ver SinForce y CosForce). Por último se puede especificar una lista de entereos que representan los IDs
 *     de las partículas a las cuales se les aplica la fuerza en cuestión, en caso de no poner una lista de IDs, la
 *     fuerza se aqplica a TODAS las partículas
 *     - <b>\# velocity: name,double,double,double[,double,double,double]?[,integer]*</b> Idem a las fuerzas sólo que aquí se
 *     especifica una velocidad constante, sinusoidal o cosinusoidal.
 *  * Salida:
 *     - <b>\# record: name,path,integer</b> define el formato de salida de los datos de la simulación. <b>name</b>
 *     puede ser <b>txt</b> o <b>vtk</b>. Para el caso de salida de texto <b>path</b> es el fichero donde se deben
 *     guardar los datos ejemplo "output/run1.txt". Para salida VTK, <b>path</b> es un directorio donde se guardarán los
 *     datos de salida para vtk, tiene que ser un directorio porque los datos para vtk constan de varios ficheros. El
 *     directorio puede no existir, en cuyo caso se creará. Finalmente se especifica un entero que representa cada
 *     cuantas iteraciones se guardarán los datos. Otra utilidad que se incluye en esta propiedad es la de generar un
 *     reporte visual del porcentaje de la simulación que se ha copletado hasta el momento en est caso basta con poner
 *     <b># record: progress</b>
 *
 *  ## Propiedades de materiales e interfaces ##
 *  * La sección que define los materiales se encabeza con la línea <b>[material]</b>, a continuación se especifican las
 *  columnas de datos que se introducen en un formato CSV (Comma Separated Value). Las columnas básicas son
 *  <b>model,knc,knt,kt,rn,rt,coulomb</b> que definen el modelo que maneja las itneracciones para este tipo de material, y
 *  las constantes propies del material. Puede haber columnas extras para los modelos que necesiten datos mas complejos
 *  en sus materiales. Los IDs se asignan según el orden en que se especifican los materiales empezando por 0 e
 *  icrementando en 1 para cada material.
 *  * Las interfaces son el mecanismo que define como manejar la interacción entre partículas de diferentes materiales.
 *  Así se puede definir cual de los dos modelos (uno de cada material diferente) se va a encargar de la interacción mixta
 *  o si se va a utilizar un modelo específico para la combinacion de materiales que representa la interfaz.
 *  La interfaces se especifican siguiendo el formato CSV con una primera linea <b>[interface]</b> que define esta sección
 *  del fichero de entrada. A continuación la línea que define las columnas de los datos que siguen y luego una linea para
 *  cada iterfaz. Las columnas para los datos deben ser <b>materialId1,materialId2,model,rn,rt</b> donde se define para
 *  un par de materiales el modelo a utilizar y las constantes de ruptura específicas par esta interfaz. Puede haber
 *  columnas extras.
 *
 *  ## Propiedades de las partículas ##
 *  Para las partículas hay varias secciones, una para cada forma de partícula, así la seccion para las partículas
 *  esféricas comienza con <b>[spheres]</b> y la de los triángulos por <b>[triangle]</b>. A continuación se coloca una
 *  linea con las columnas que especifican el orden de los datos para cada partícula.
 *
 *  * Propiedades para las esferas: <b>center_x,center_y,center_z,radius,mass,inertiaMoment_xx,inertiaMoment_xy,
 *  inertiaMoment_xz,inertiaMoment_yx,inertiaMoment_yy,inertiaMoment_yz,inertiaMoment_zx,inertiaMoment_zy,inertiaMoment_zz,
 *  mobile,fixedPoint_x,fixedPoint_y,fixedPoint_z,material</b>
 *
 *   - center_x,center_y,center_z: las coordenadas del centro de la partícula
 *   - radius,mass: radio y masa de la partícula. El radio es el mínimo que permite acotar la partícula dentro de una
 *   esfera con centro en el centro de la partícula.
 *   - inertiaMoment_ab: son los componenetes del tensor de inercia de la partícula al inicio de la simulación (a,b
 *   recorren {x,y,z})
 *   - mobile: indica si la particula es movil. Esto afecta la integración de la posición de la partícula, así las
 *   partículas inmóviles no se les integra la posición; pueden ser movidas pero no por medio de la integración de fuerzas
 *   sino por algun mecanismo externo como ConstantVelocity o SinVelocity etc.
 *   - fixedPoint_x,fixedPoint_y,fixedPoint_z: coordenadas de un punto fijo en la superficie de la partícula. Es usado
 *   para visualizar la rotación de las partículas.
 *   - material: Id del material de la partícula. Esto define el modelo que se usa para manejar las interacciones.
 *
 *   * Propiedades para los triángulos: <b>center_x,center_y,center_z,mobile,v0_x,v0_y,v0_z,v1_x,v1_y,v1_z,v2_x,v2_y,
 *   v2_z,material</b>
 *
 *    - center_x,center_y,center_z: las coordenadas del centro del triangulo. Puede ser el centro de masas u otro punto
 *    dentro del triangulo.
 *    - mobile: define si el triángulo es movil. Por ahora todos lo striángulos deben ser inmóviles porque no se ha
 *    implementado un modelo de interacción que le asigne fuerzas de interacción a los triángulos, pero se puede hacer
 *    y así funcionarían como una partícula mas.
 *    - vi_x,vi_y,vi_z: coordenadas de los vectores que definen los vértices del triángulo, así el vértice Vi sería
 *    Vi=Center+(vi_x,vi_y,vi_z).
 *    - material: define el material del triángulo, típicamente un material que representa una pared en la simulación.
 *
 */
class CsvSimulationLoader {
public:
	CsvSimulationLoader(string fileName);
	virtual ~CsvSimulationLoader();
	Simulation* read();
private:

	void processGlobalLine();
	void processParticleLine(int type);
	void processInteractionLine();
	void processInterfaceLine();
	void processMaterialLine();
	boost::shared_ptr<Shape> makeSphere();
	boost::shared_ptr<Shape> makePlane();
	boost::shared_ptr<Shape> makeTriangle();
	void processHeaders();
	void processValues();
	void initDefaultValues();

	string line,file;
	Simulation* sim;
	vector<string> headers;
	map<string,double> values;
	Real xMax,yMax,zMax, xMin,yMin,zMin;
};

#endif /* CSVSIMULATIONLOADER_H_ */
