/*
 * MathUtils.cpp
 *
 *  Created on: may 30, 2014
 *      Author: Alvaro Javier
 */

#include "MathUtils.h"

MatrixNN MathUtils::getRotationMatrix(const boost::shared_ptr<Particle>& p, Simulation* sim) {
	//todos los calculos a continuación son extraídos de la tesis de Irvin ep 3.6.1.2

	//este vector presenta el cambio de rotación en el intervalo de tiempo
	VectorND dc = p->getAngularVelocity() * sim->getTimeStep();
	//matriz auxiliar
	MatrixNN dcn;
	dcn << 0, -dc.z(), dc.y(), dc.z(), 0, -dc.x(), -dc.y(), dc.x(), 0;
	double dcnorm = dc.norm();
	//matriz de actualización
	MatrixNN rCN = MatrixNN::Identity();
	if (dcnorm > MathDefs::EPSILON) { //si la norma del vector es >0
		rCN = cos(dcnorm) * MatrixNN::Identity() + (sin(dcnorm)/dcnorm) * dcn + ((1-cos(dcnorm))/(dcnorm*dcnorm)) * (dc*dc.transpose());
	}
	return rCN;
}

