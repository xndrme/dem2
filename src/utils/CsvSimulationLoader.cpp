/*
 * CsvSimulationLoader.cpp
 *
 *  Created on: ene 18, 2015
 *      Author: xndrme
 */

#include "CsvSimulationLoader.h"
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <forces/Gravity.h>
#include <integrators/IntegratorEngine.h>
#include <integrators/HouboltIntegrator.h>
#include <integrators/GearIntegrator.h>
#include <collisions/MGCDCollisionDetector.h>
#include <collisions/BruteForceCollisionDetector.h>
#include <models/DefaultInteractionsModel.h>
#include <models/CohesiveFrictionalOverTime.h>
#include <models/IrvinModelWithDamping.h>
#include <models/WallModel.h>
#include <models/WallModelCohesive.h>
#include <utils/TxtRecorder.h>
#include <shapes/Triangle.h>
#include "VtkRecorder.h"
#include "ProgressReporter.h"
#include <models/HertzModel.h>
#include <fstream>
#include <forces/ConstantForce.h>
#include <forces/CosForce.h>
#include <forces/SinForce.h>
#include <forces/ConstantVelocity.h>
#include <forces/CosVelocity.h>
#include <forces/SinVelocity.h>

using namespace std;

CsvSimulationLoader::CsvSimulationLoader(string fileName) : file(fileName),sim(NULL),headers(vector<string>()),
	xMax(0),yMax(0),zMax(0),xMin(0),yMin(0),zMin(0) {
	Material::lastId = -1;

}

CsvSimulationLoader::~CsvSimulationLoader() {}

void CsvSimulationLoader::processGlobalLine() {
	vector<string> tokens;
	boost::split(tokens,line,boost::is_any_of("#:"));
	string key = tokens[1],value = tokens[2];
	if		(key=="minr")				sim->setMinimumRadius(::atof(value.c_str()));
	else if	(key=="maxr") 				sim->setMaximumRadius(::atof(value.c_str()));
	else if	(key=="xmin") 				xMin = ::atof(value.c_str());
	else if	(key=="ymin") 				yMin = ::atof(value.c_str());
	else if	(key=="zmin") 				zMin = ::atof(value.c_str());
	else if	(key=="xmax") 				xMax = ::atof(value.c_str());
	else if	(key=="ymax") 				yMax = ::atof(value.c_str());
	else if	(key=="zmax") 				zMax = ::atof(value.c_str());
	else if	(key=="fdamp") 				sim->setForceDamping(::atof(value.c_str()));
	else if	(key=="tdamp") 				sim->setTorqueDamping(::atof(value.c_str()));
    else if	(key=="viscous") 			sim->setViscous(::atoi(value.c_str()));
	else if	(key=="step")				sim->setTimeStep(::atof(value.c_str()));
	else if	(key=="iters")				sim->setNSteps(::atoi(value.c_str()));
	else if	(key=="cohesivedistance")	sim->setCohesiveDistance(::atof(value.c_str()));
	else if	(key=="gravacc") {
		boost::shared_ptr<GeneralPurposeEngine> ge(new Gravity(VectorND(0, 0, -::atof(value.c_str()))));
										sim->getGeneralPurposeEngines().push_back(ge);
	}
	else if	(key=="force") {
		vector<string> conf;
		boost::split(conf,value,boost::is_any_of(","));
		boost::shared_ptr<GeneralPurposeEngine> force;
		if(conf[0]=="constant") {
			VectorND f = VectorND(::atof(conf[1].c_str()),::atof(conf[2].c_str()),::atof(conf[3].c_str()));
            if(conf.size()==4) {
                force = boost::shared_ptr<GeneralPurposeEngine>(new ConstantForce(f));
            } else {
                vector<int> toParts;
                for(int i=4; i<conf.size();i++) {
                    toParts.push_back(::atoi(conf[i].c_str()));
                }
                force = boost::shared_ptr<GeneralPurposeEngine>(new ConstantForce(f,toParts));
            }
		} else if(conf[0]=="cos" || conf[0]=="sin") {
            VectorND a = VectorND(::atof(conf[1].c_str()),::atof(conf[2].c_str()),::atof(conf[3].c_str()));
            VectorND b = VectorND(::atof(conf[4].c_str()),::atof(conf[5].c_str()),::atof(conf[6].c_str()));
            if(conf.size()==7) {
                if(conf[0]=="cos")
                    force = boost::shared_ptr<GeneralPurposeEngine>(new CosForce(a,b));
                else
                    force = boost::shared_ptr<GeneralPurposeEngine>(new SinForce(a,b));
            } else {
                vector<int> toParts;
                for(int i=7; i<conf.size();i++) {
                    toParts.push_back(::atoi(conf[i].c_str()));
                }
                if(conf[0]=="cos")
                    force = boost::shared_ptr<GeneralPurposeEngine>(new CosForce(a,b,toParts));
                else
                    force = boost::shared_ptr<GeneralPurposeEngine>(new SinForce(a,b,toParts));
            }
        }
		sim->getGeneralPurposeEngines().push_back(force);
	} else if (key=="velocity") {
        vector<string> conf;
        boost::split(conf,value,boost::is_any_of(","));
        boost::shared_ptr<GeneralPurposeEngine> vel;
        if (conf[0] == "constant") {
            VectorND v = VectorND(::atof(conf[1].c_str()), ::atof(conf[2].c_str()), ::atof(conf[3].c_str()));
            if (conf.size() == 4) {
                vel = boost::shared_ptr<GeneralPurposeEngine>(new ConstantVelocity(v));
            } else {
                vector<int> toParts;
                for (int i = 4; i < conf.size(); i++) {
                    toParts.push_back(::atoi(conf[i].c_str()));
                }
                vel = boost::shared_ptr<GeneralPurposeEngine>(new ConstantVelocity(v, toParts));
            }
        } else if(conf[0]=="cos" || conf[0]=="sin") {
			VectorND a = VectorND(::atof(conf[1].c_str()),::atof(conf[2].c_str()),::atof(conf[3].c_str()));
			VectorND b = VectorND(::atof(conf[4].c_str()),::atof(conf[5].c_str()),::atof(conf[6].c_str()));
			if(conf.size()==7) {
				if(conf[0]=="cos")
					vel = boost::shared_ptr<GeneralPurposeEngine>(new CosVelocity(a,b));
				else
					vel = boost::shared_ptr<GeneralPurposeEngine>(new SinVelocity(a,b));
			} else {
				vector<int> toParts;
				for(int i=7; i<conf.size();i++) {
					toParts.push_back(::atoi(conf[i].c_str()));
				}
				if(conf[0]=="cos")
					vel = boost::shared_ptr<GeneralPurposeEngine>(new CosVelocity(a,b,toParts));
				else
					vel = boost::shared_ptr<GeneralPurposeEngine>(new SinVelocity(a,b,toParts));
			}
		}
        sim->getGeneralPurposeEngines().push_back(vel);
    } else if (key=="record") {
		vector<string> conf;
		boost::split(conf,value,boost::is_any_of(","));
		boost::shared_ptr<GeneralPurposeEngine> rec;
		if(conf[0]=="txt") {
			rec = boost::shared_ptr<GeneralPurposeEngine>(new TxtRecorder(conf[1], ::atoi(conf[2].c_str())));
		} else if(conf[0]=="vtk") {
			rec = boost::shared_ptr<GeneralPurposeEngine>(new VtkRecorder(conf[1], ::atoi(conf[2].c_str())));
		} else if(conf[0]=="progress") {
			rec = boost::shared_ptr<GeneralPurposeEngine>(new ProgressReporter());
		}
			sim->getGeneralPurposeEngines().push_back(rec);

	}
	else if	(key=="models") {
		vector<string> conf;
		boost::split(conf,value,boost::is_any_of(","));
		boost::shared_ptr<ModelEngine> model;
		for(unsigned int i=0;i<conf.size();++i) {
			if(conf[i]=="default") {
				model = boost::shared_ptr<ModelEngine>(new DefaultInteractionsModel());
			} else if (conf[i]=="wall") {
				model = boost::shared_ptr<ModelEngine>(new WallModel());
			} else if (conf[i]=="wallcohesive") {
				model = boost::shared_ptr<ModelEngine>(new WallModelCohesive());
			} else if (conf[i]=="cohesivefrictionalovertime") {
				model = boost::shared_ptr<ModelEngine>(new CohesiveFrictionalOverTime());
			} else if (conf[i]=="irvindamping") {
				model = boost::shared_ptr<ModelEngine>(new IrvinModelWithDamping());
			} else if (conf[i]=="hertz") {
				model = boost::shared_ptr<ModelEngine>(new HertzModel());
			}

			sim->getModels().push_back(model);
		}
	}
	else if	(key=="coll") {
		if(boost::starts_with(value,"mgcd")) {
			vector<string> conf;
			boost::split(conf,value,boost::is_any_of(","));
			MGCDCollisionDetector* coll= new MGCDCollisionDetector();
			coll->setAlgConf(conf[conf.size()-1]);
			Float grids[100];
			for(unsigned int i=1;i<conf.size()-1;i++) {
				grids[i-1] = (Float) (::atof(conf[i].c_str()));
			}
			coll->setGrids(grids, conf.size()-2);
										sim->setCollisionEngine(boost::shared_ptr<Engine>(coll));
		}
	}
	else if	(key=="integrator")	{
		boost::shared_ptr<IntegratorEngine> ie;
		if(value=="houbolt") {
			ie = boost::shared_ptr<IntegratorEngine>(new HouboltIntegrator());
			//El integrador de Houbolt necesita manejar info extra así que lo ponemos como general purpose
										sim->getGeneralPurposeEngines().push_back(ie);
		} else if (value=="gear") {
			ie = boost::shared_ptr<IntegratorEngine>(new GearIntegrator());
			//El integrador de Gear necesita manejar info extra así que lo ponemos como general purpose
										sim->getGeneralPurposeEngines().push_back(ie);
		}
										sim->setIntegrationEngine(ie);
	}
}

void CsvSimulationLoader::processInterfaceLine() {
	processValues();

	//vreificar si la matriz de interfaces no está inicializada
	if(sim->getInterfaces().size()<sim->getMaterials().size()) {
		//inicializarla con un vector para cada fila
		//el número de filas es igual al número de materiales
		for(unsigned int i=0;i<sim->getMaterials().size();++i) {
			//cada fila tiene longitud igual a la cantidad de materiales
			sim->getInterfaces().push_back(
					std::vector<Interface>(sim->getMaterials().size(), Interface(
							sim->getMaterials()[i]->id,
							sim->getMaterials()[i]->rn,
							sim->getMaterials()[i]->rt
							))
					);
		}
	}

	sim->getInterfaces()[values["materialid1"]][values["materialid2"]] = Interface(values["model"],values["rn"],values["rt"]);
	sim->getInterfaces()[values["materialid2"]][values["materialid1"]] = Interface(values["model"],values["rn"],values["rt"]);
}

void CsvSimulationLoader::processMaterialLine() {
	processValues();
	boost::shared_ptr<Material> m = boost::shared_ptr<Material>(sim->getModels()[values["model"]]->getMaterialInstance());
	m->modelId = values["model"];
	m->knc = values["knc"];
	m->knt = values["knt"];
	m->kt = values["kt"];
    m->rn = values["rn"];
    m->rt = values["rt"];
    m->coulomb = values["coulomb"];
    for(map<string, Real>::const_iterator it = values.begin();it!=values.begin();++it) {
    	m->setProperty(it->first,it->second);
    }
	sim->getMaterials().push_back(m);
}

void CsvSimulationLoader::processParticleLine(int type) {
	processValues();
	boost::shared_ptr<Particle> part(new Particle());
	boost::shared_ptr<Shape> shape;
	MatrixNN inertia;
	switch (type) {
		case SPHERE_TYPE:
			shape = makeSphere();
			part->setFixedPoint(VectorND(values["fixedpoint_x"],values["fixedpoint_y"],values["fixedpoint_z"]));
			part->setCenterBefore(part->getCenter()); //al inicio de la simulacion el centro anterior es el mismo
			part->setMass(values["mass"]);
			inertia <<	values["inertiamoment_xx"],values["inertiamoment_xy"],values["inertiamoment_xz"],
						values["inertiamoment_yx"],values["inertiamoment_yy"],values["inertiamoment_yz"],
						values["inertiamoment_zx"],values["inertiamoment_zy"],values["inertiamoment_zz"];
			part->setInertiaMoment(inertia);
			part->setInverseInertiaMoment(inertia.inverse());
			break;
		case PLANE_TYPE:
			shape = makePlane();
			break;
		case TRIANGLE_TYPE:
			shape = makeTriangle();
			break;
		default:
			break;
	}
	part->setCenter(VectorND(values["center_x"],values["center_y"],values["center_z"]));
	part->setShape(shape);
	part->setMobile(values["mobile"]>0);
	part->setMaterialId(values["material"]);

	//parámetros opcionales
	if(values.find("velocity_x")!=values.end() &&
		values.find("velocity_y")!=values.end() &&
		values.find("velocity_z")!=values.end()) {
		part->setVelocity(VectorND(values["velocity_x"],values["velocity_y"],values["velocity_z"]));
	}

	sim->getParticles().insert(part);

}


boost::shared_ptr<Shape> CsvSimulationLoader::makeSphere() {
	boost::shared_ptr<Sphere> s(new Sphere());

	s->setRadius(values["radius"]);

	return s;
}

boost::shared_ptr<Shape> CsvSimulationLoader::makePlane() {
	boost::shared_ptr<Plane> s(new Plane());

	s->setNormal(VectorND(values["normal_x"],values["normal_y"],values["normal_z"]));

	return s;
}

boost::shared_ptr<Shape> CsvSimulationLoader::makeTriangle() {
	boost::shared_ptr<Triangle> s(new Triangle());

	s->setV0(VectorND(values["v0_x"],values["v0_y"],values["v0_z"]));
	s->setV1(VectorND(values["v1_x"],values["v1_y"],values["v1_z"]));
	s->setV2(VectorND(values["v2_x"],values["v2_y"],values["v2_z"]));

	return s;
}

void CsvSimulationLoader::processInteractionLine() {
	processValues();
	boost::shared_ptr<Interaction> i(new Interaction(values["id1"],values["id2"]));
	i->setCohesive(values["cohesive"]>0);
	i->setSpringPoint1(VectorND(values["spring_x"],values["spring_y"],values["spring_z"]));
	i->setSpringPoint2(VectorND(values["spring_x"],values["spring_y"],values["spring_z"]));
	sim->getInteractions().insert(i);
}
void CsvSimulationLoader::processHeaders() {
	headers.clear();
	values.clear();
	boost::split(headers, line, boost::is_any_of(","));
}
void CsvSimulationLoader::processValues() {
	vector<string> str_values;
	boost::split(str_values, line, boost::is_any_of(","));
	for(unsigned int i=0; i<headers.size(); i++ ) {
		values[headers[i]] = ::atof(str_values[i].c_str());
	}
}

void CsvSimulationLoader::initDefaultValues() {
	sim->setCollisionEngine(boost::shared_ptr<Engine>(new BruteForceCollisionDetector()));
}

Simulation* CsvSimulationLoader::read() {
	sim = new Simulation();

	initDefaultValues();

	std::ifstream in(file.c_str(), ios::in);
	int type = 0; //tipo de dato que se lee
	while(getline(in, line)) {
		if(line.empty()) continue;
		boost::erase_all(line, " ");
		boost::to_lower(line);
		switch (line[0]) {
			case '#':
				processGlobalLine();
				break;
			case '[':
				if(line=="[sphere]" || line=="[spheres]") 					type = SPHERE_TYPE;
				else if(line=="[plane]" || line=="[planes]") 				type = PLANE_TYPE;
				else if(line=="[interaction]" || line=="[interactions]") 	type = INTERACTION_TYPE;
				else if(line=="[triangle]" || line=="[triangles]")			type = TRIANGLE_TYPE;
				else if(line=="[interface]" || line=="[interfaces]")		type = INTERFACE_TYPE;
				else if(line=="[material]" || line=="[materials]")			type = MATERIAL_TYPE;
				getline(in, line);
				boost::erase_all(line, " ");
				boost::to_lower(line);
				processHeaders();
				break;
			default:
				switch (type) {
					case INTERACTION_TYPE:
						processInteractionLine();
						break;
					case INTERFACE_TYPE:
						processInterfaceLine();
						break;
					case MATERIAL_TYPE:
						processMaterialLine();
						break;
					case SPHERE_TYPE:
					case TRIANGLE_TYPE:
						processParticleLine(type);
						break;
				}
				break;
		}
	}

	sim->setSimulationBounds(AABB(VectorND(xMin,yMin,zMin),VectorND(xMax,yMax,zMax)));
	return sim;
}


