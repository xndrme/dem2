//
// Created by Alvaro Javier on 7/1/2015.
//

#include <stdio.h>
#include <time.h>
#include "ProgressReporter.h"

void ProgressReporter::beforeStep(Simulation *sim) {
    if(iters == 0) {
        iters = sim->getNsteps()/100;
    }
    if(sim->getIter() == 0) {
        startTime = time(NULL);
    }
    if(sim->getIter() % iters == 0) {
        printf("Completado %d%%\n",sim->getIter()/iters);
    }
    if(sim->getIter() == sim->getNsteps()-1) {
        endTime = time(NULL);
        //hay que agregarle el aproximado de cuanto de debe demorar la ultima iteracion
        //ya que no hay forma de reportar el tiempo final exactamente.
        endTime += (endTime-startTime)/sim->getNsteps();
        int totalSeg = endTime - startTime;
        printf("Tiempo total de simulacion %d:%d:%d\n",totalSeg/3600,(totalSeg % 3600)/60, totalSeg % 60);
        printf("Total de pasos de simulacion %d\n",sim->getNsteps());
        int prom = totalSeg/sim->getNsteps();
        printf("Tiempo promedio para un paso <%d seg\n",prom+1);
    }
}
