//
// Created by xndrme on 18/05/2015.
//

#include "VtkRecorder.h"

#include <vtkUnstructuredGrid.h>
#include <vtkTriangle.h>
#include <vtkPointData.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>

#include <shapes/Sphere.h>
#include <shapes/Triangle.h>

#include <boost/format.hpp>

using namespace std;

VtkRecorder::VtkRecorder(const std::string& filePath, int iterStep) : iter(iterStep) {
    dir = boost::filesystem::path(filePath);
    if(!boost::filesystem::create_directory(dir) && !boost::filesystem::is_directory(dir)) {
        runtime_error(("CsvLoader:: Cannot create the dir: " + dir.string()).c_str());
    }
}

VtkRecorder::~VtkRecorder() {}

void VtkRecorder::beforeStep(Simulation *sim) {

    unsigned int curr_iter = sim->getIter();
    if (curr_iter % iter != 0) {
        return;
    }

    saveSpheres(sim);
    saveTriangles(sim);
}

void VtkRecorder::saveTriangles(Simulation *sim) {

    vtkSmartPointer<vtkPoints> vertexs = vtkSmartPointer<vtkPoints>::New();

    vtkSmartPointer<vtkDoubleArray> forces = vtkSmartPointer<vtkDoubleArray>::New();
    forces->SetNumberOfComponents(3);
    forces->SetName("forces");

    vtkSmartPointer<vtkDoubleArray> velocities = vtkSmartPointer<vtkDoubleArray>::New();
    velocities->SetNumberOfComponents(3);
    velocities->SetName("velocities");

    vtkSmartPointer<vtkDoubleArray> ids = vtkSmartPointer<vtkDoubleArray>::New();
    ids->SetNumberOfComponents(1);
    ids->SetName("ids");

    vtkSmartPointer<vtkDoubleArray> materials = vtkSmartPointer<vtkDoubleArray>::New();
    materials->SetNumberOfComponents(1);
    materials->SetName("materials");

    vtkSmartPointer<vtkUnstructuredGrid> trianglesGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

    boost::shared_ptr<Particle> p;
    boost::shared_ptr<Triangle> t;
    for(unsigned int i=0; i < sim->getParticles().size(); i++) {
        p = sim->getParticles()[i];
        t = boost::dynamic_pointer_cast<Triangle>(p->getShape());
        if(!t) continue;

        VectorND point0 = t->getV0() + p->getCenter();
        VectorND point1 = t->getV1() + p->getCenter();
        VectorND point2 = t->getV2() + p->getCenter();
        vertexs->InsertPoint(3*i,point0.data());
        vertexs->InsertPoint(3*i+1,point1.data());
        vertexs->InsertPoint(3*i+2,point2.data());

        vtkSmartPointer<vtkTriangle> tri = vtkSmartPointer<vtkTriangle>::New();

        tri->GetPointIds()->SetId(0,3*i);
        tri->GetPointIds()->SetId(1,3*i+1);
        tri->GetPointIds()->SetId(2,3*i+2);

        trianglesGrid->InsertNextCell(tri->GetCellType(), tri->GetPointIds());

        double handy[1];//utilitario para guardar escalares
        //guardar fuerza
        forces->InsertNextTuple(sim->getForces().getForce(p->getId()).data());
        //guardar velocidad
        velocities->InsertNextTuple(p->getVelocity().data());
        //guardar ID
        handy[0] = p->getId(); ids->InsertNextTuple(handy);
        //guardar material
        handy[0] = p->getMaterialId();  materials->InsertNextTuple(handy);

    }

    trianglesGrid->SetPoints(vertexs);

    trianglesGrid->GetCellData()->AddArray(forces);
    trianglesGrid->GetCellData()->AddArray(velocities);
    trianglesGrid->GetCellData()->AddArray(ids);
    trianglesGrid->GetCellData()->AddArray(materials);

    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();

    string out = (boost::format("%s/triangles%03d.vtu") % dir.string() % (sim->getIter()/iter)).str();
    writer->SetFileName(out.c_str());
    writer->SetInputData(trianglesGrid);
    writer->Write();

}

void VtkRecorder::saveSpheres(Simulation *sim) {

    vtkSmartPointer<vtkPoints> centers = vtkSmartPointer<vtkPoints>::New();

    vtkSmartPointer<vtkDoubleArray> radii = vtkSmartPointer<vtkDoubleArray>::New();
    radii->SetNumberOfComponents(1);
    radii->SetName("radii");

    vtkSmartPointer<vtkDoubleArray> forces = vtkSmartPointer<vtkDoubleArray>::New();
    forces->SetNumberOfComponents(3);
    forces->SetName("forces");

    vtkSmartPointer<vtkDoubleArray> velocities = vtkSmartPointer<vtkDoubleArray>::New();
    velocities->SetNumberOfComponents(3);
    velocities->SetName("velocities");

    vtkSmartPointer<vtkDoubleArray> fixed_points = vtkSmartPointer<vtkDoubleArray>::New();
    fixed_points->SetNumberOfComponents(3);
    fixed_points->SetName("fixed_points");

    vtkSmartPointer<vtkDoubleArray> ids = vtkSmartPointer<vtkDoubleArray>::New();
    ids->SetNumberOfComponents(1);
    ids->SetName("ids");

    vtkSmartPointer<vtkDoubleArray> materials = vtkSmartPointer<vtkDoubleArray>::New();
    materials->SetNumberOfComponents(1);
    materials->SetName("materials");

    boost::shared_ptr<Particle> p;
    boost::shared_ptr<Sphere> s;
    for(unsigned int i=0; i < sim->getParticles().size(); i++) {
        p = sim->getParticles()[i];
        s = boost::dynamic_pointer_cast<Sphere>(p->getShape());
        if(!s) continue;
        //guardar el centro
        centers->InsertNextPoint(p->getCenter().data());

        double handy[1]; //utilitario para guardar escalares
        //guardar radio
        handy[0] = s->getRadius(); radii->InsertNextTuple(handy);
        //guardar fuerza
        forces->InsertNextTuple(sim->getForces().getForce(p->getId()).data());
        //guardar velocidad
        velocities->InsertNextTuple(p->getVelocity().data());
        //guardar punto fijo
        fixed_points->InsertNextTuple(p->getFixedPoint().data());
        //guardar ID
        handy[0] = p->getId();
        ids->InsertNextTuple(handy);
        //guardar material
        handy[0] = p->getMaterialId();
        materials->InsertNextTuple(handy);
    }

    vtkSmartPointer<vtkUnstructuredGrid> spheresGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

    spheresGrid->SetPoints(centers);
    spheresGrid->GetPointData()->AddArray(radii);
    spheresGrid->GetPointData()->AddArray(forces);
    spheresGrid->GetPointData()->AddArray(velocities);
    spheresGrid->GetPointData()->AddArray(fixed_points);
    spheresGrid->GetPointData()->AddArray(ids);
    spheresGrid->GetPointData()->AddArray(materials);

    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();

    string out = (boost::format("%s/spheres%03d.vtu") % dir.string() % (sim->getIter()/iter)).str();
    writer->SetFileName(out.c_str());
    writer->SetInputData(spheresGrid);
    writer->Write();

}
