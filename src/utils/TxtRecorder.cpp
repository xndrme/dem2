/*
 * File:   TxtRecorder.cpp
 * Author: soad
 *
 */

#include "TxtRecorder.h"
#include <shapes/Sphere.h>
#include <shapes/Triangle.h>
#include <fstream>

using namespace std;

TxtRecorder::TxtRecorder(const std::string& fileName, int iterStep) : file(fileName), iter(iterStep) {
    unsigned int idx = fileName.find(".");
    file_triangles = fileName;
    file_triangles.replace(idx,idx+4,"_triangles");
    file_triangles += fileName.substr(idx,idx+4);
}

void TxtRecorder::beforeStep(Simulation* sim) {

    unsigned int curr_iter = sim->getIter();
    if (curr_iter % iter != 0) {
        return;
    }

    FILE* out = fopen(file.c_str(),curr_iter == 0? "wt":"at");
    FILE* out_tri = fopen(file_triangles.c_str(),curr_iter == 0? "wt":"at");

    if (curr_iter == 0) {
        fprintf(out,"iter,id,rad,cx,cy,cz,fx,fy,fz,vx,vy,vz,ax,ay,az,Fx,Fy,Fz,Tx,Ty,Tz,cbx,cby,cbz,dx,dy,dz\n");
        fprintf(out_tri,"iter,id,cx,cy,cz,v0x,v0y,v0z,v1x,v1y,v1z,v2x,v2y,v2z\n");
    }

    const unsigned int size = sim->getParticles().size();
    for (unsigned int i = 0; i < size; i++) {

        const Particle* particle = sim->getParticles()[i].get();

        const Sphere* shpere = dynamic_cast<Sphere*>(particle->getShape().get());
        VectorND h;
        if (shpere) {
            fprintf(out,"%d,%d,%.8f,",curr_iter,i,shpere->getRadius());
            h = particle->getCenter();
            fprintf(out,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = particle->getFixedPoint();
            fprintf(out,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = particle->getVelocity();
            fprintf(out,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = particle->getAcceleration();
            fprintf(out,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = sim->getForces().getForce(particle->getId());
            fprintf(out,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = sim->getForces().getTorque(particle->getId());
            fprintf(out,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = particle->getCenterBefore();
            fprintf(out,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = particle->getCenter() - h;
            fprintf(out,"%.8f,%.8f,%.8f\n",h.x(),h.y(),h.z());
        }
        const Triangle* triangle = dynamic_cast<Triangle*>(particle->getShape().get());
        if (triangle) {
            fprintf(out_tri,"%d,%d,",curr_iter,i);
            h = particle->getCenter();
            fprintf(out_tri,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = triangle->getV0();
            fprintf(out_tri,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = triangle->getV1();
            fprintf(out_tri,"%.8f,%.8f,%.8f,",h.x(),h.y(),h.z());
            h = triangle->getV2();
            fprintf(out_tri,"%.8f,%.8f,%.8f\n",h.x(),h.y(),h.z());
        }
    }

    fclose(out);
    fclose(out_tri);
}

TxtRecorder::~TxtRecorder() {
}

