//
// Created by xndrme on 18/05/2015.
//

#ifndef DEM2_VTKRECORDER_H
#define DEM2_VTKRECORDER_H

#include <core/GeneralPurposeEngine.h>
#include <boost/filesystem.hpp>

/**
 * @brief Guarda el estado de la simulación en cada paso de tiempo en el formato de vtk.
 */
class VtkRecorder : public GeneralPurposeEngine {

    ///directorio donde se van a guardar los datos de salida de cada paso de iteración
    boost::filesystem::path dir;
    ///Cantidad de iteraciones entre cada paso para guardar el estado de la simulación
    int iter;

public:
    VtkRecorder(const std::string& filePath, int iterStep);
    virtual ~VtkRecorder();

    void beforeStep(Simulation* sim);

    /**
     * Este método guarda los datos sobre las partículas esféricas
     */
    void saveSpheres(Simulation* sim);

    /**
     * Este método guarda los datos sobre las partículas con forma de triángulo
     */
    void saveTriangles(Simulation* sim);
};


#endif //DEM2_VTKRECORDER_H
