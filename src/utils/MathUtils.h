/*
 * MathUtils.cpp
 *
 *  Created on: may 30, 2014
 *      Author: Alvaro Javier
 */

#include <core/MathDefs.h>
#include <core/Particle.h>
#include <core/Simulation.h>

/**
 * @brief Útiles de matemática
 */
namespace MathUtils {
	/**
	 * Calcula la matriz de rotación para los puntos sobre una partícula
	 */
	MatrixNN getRotationMatrix(const boost::shared_ptr<Particle>& p, Simulation* sim);
}
