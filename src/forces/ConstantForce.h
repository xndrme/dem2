/*
 * ConstantForce.h
 *
 *  Created on: Mar 4, 2015
 *      Author: alvarojavier
 */

#ifndef CONSTANTFORCE_H_
#define CONSTANTFORCE_H_

#include <vector>

#include "GeneralForce.h"

/**
 * @brief Esta clase aplica una fuerza constante a las partículas.
 */
class ConstantForce: public GeneralForce {
public:
    /**
     * Constructor que recibe un vector que representa la fuerza a aplicar a las partículas.
     *
     * @param force: vector de fuerza
     */
	ConstantForce(VectorND force);
    /**
     * Constructor que recibe un vector que representa la fuerza a aplicar a las partículas y
     * una lista de IDs de partículas a las cuales se le aplica la fuerza.
     *
     * @param force: vector de fuerza
     * @param toParts: lista de IDs de partículas a las cuales se le aplica la fuerza
     */

    ConstantForce(VectorND force, std::vector<int> toParts);
	virtual ~ConstantForce();
	virtual void applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation* sim);
private:
	VectorND force;
};

#endif /* CONSTANTFORCE_H_ */
