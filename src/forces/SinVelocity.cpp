//
// Created by Alvaro Javier on 6/12/2015.
//

#include "SinVelocity.h"

void SinVelocity::applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation *sim) {
    part->setCenterBefore(part->getCenter());
    const VectorND &v = VectorND(
            a.x() * sin(fmod(b.x() * sim->getTime(), M_2_PI)),
            a.y() * sin(fmod(b.y() * sim->getTime(), M_2_PI)),
            a.z() * sin(fmod(b.z() * sim->getTime(), M_2_PI))
    );
    part->setCenter(part->getCenter() + v);
    part->setFixedPoint(part->getFixedPoint() + v);
}
