//
// Created by Alvaro Javier on 6/12/2015.
//

#include "SinForce.h"

void SinForce::applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation *sim) {
    VectorND force = VectorND(
        a.x()*sin(fmod(b.x()*sim->getTime(),M_2_PI)),
        a.y()*sin(fmod(b.y()*sim->getTime(),M_2_PI)),
        a.z()*sin(fmod(b.z()*sim->getTime(),M_2_PI))
    );
    sim->getForces().addForce(partIndex,force);
}
