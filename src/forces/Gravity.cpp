/* 
 * File:   GravityEngine.cpp
 * Author: soad
 * 
 * Created on June 25, 2011, 1:06 PM
 */

#include "Gravity.h"

using namespace std;
using boost::shared_ptr;

Gravity::Gravity(VectorND gravity) : G(gravity) {
}

void Gravity::beforeIntegration(Simulation* sim) {
	const ParticleContainer& particles = sim->getParticles();

	const unsigned int size = particles.size();
	for (unsigned int i = 0; i < size; i++)
		sim->getForces().addForce(i, particles[i]->getMass() * G);
}

Gravity::~Gravity() {
}

