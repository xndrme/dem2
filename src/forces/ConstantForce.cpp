/*
 * ConstantForce.cpp
 *
 *  Created on: Mar 4, 2015
 *      Author: alvarojavier
 */

#include "ConstantForce.h"

ConstantForce::ConstantForce(VectorND force) : force(force) {
}

ConstantForce::~ConstantForce() {
}

ConstantForce::ConstantForce(VectorND force, std::vector<int> toParts) : GeneralForce(toParts), force(force) {
}

void ConstantForce::applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation* sim) {
	sim->getForces().addForce(partIndex,force);
}
