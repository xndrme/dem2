/*
 * ConstantVelocity.cpp
 *
 *  Created on: 07/04/2015
 *      Author: xndrme
 */

#include "ConstantVelocity.h"

ConstantVelocity::ConstantVelocity(VectorND vel) : velocity(vel) {}

ConstantVelocity::~ConstantVelocity() {
}

ConstantVelocity::ConstantVelocity(VectorND vel, std::vector<int> toParts) : GeneralForce(toParts), velocity(vel) {
}

void ConstantVelocity::applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation* sim) {
    const VectorND d = sim->getTimeStep() * velocity;
    const VectorND c = part->getCenter();
    part->setCenterBefore(c);
    part->setCenter(c + d);
	part->setFixedPoint(part->getFixedPoint() + d);
}
