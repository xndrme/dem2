//
// Created by Alvaro Javier on 6/12/2015.
//

#ifndef DEM2_COSFORCE_H
#define DEM2_COSFORCE_H


#include "GeneralForce.h"

/**
 * @brief  Esta clase implementa la aplicación de una fuerza a las partículas siguiendo la función coseno
 *
 * Según la fórmula: f=a*cos(bt)
 */
class CosForce : public GeneralForce {

public:
    /**
     * Este constructor recibe los valores de los coeficientes a y b:  f=a*cos(bt)
     *
     * @param a: coeficientes para el vector a
     * @param b: coeficientes para el vector b
     */
    CosForce(VectorND a, VectorND b) : a(a),b(b) {}

    /**
     * Este constructor recibe los valores de los coeficientes a y b. Además espera una lista de IDs de partículas a las
     * cuales aplicarle la fuerza. f=a*cos(bt)
     *
     * @param a: coeficientes para el vector a
     * @param b: coeficientes para el vector b
     */
    CosForce(VectorND a, VectorND b, const std::vector<int> &toParticles) : GeneralForce(toParticles), a(a),b(b) {}
    virtual ~CosForce() { }

    virtual void applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation* sim);

private:
    VectorND a,b;

};


#endif //DEM2_COSFORCE_H
