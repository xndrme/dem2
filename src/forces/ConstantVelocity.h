/*
 * ConstantVelocity.h
 *
 *  Created on: 07/04/2015
 *      Author: xndrme
 */

#ifndef FORCES_CONSTANTVELOCITY_H_
#define FORCES_CONSTANTVELOCITY_H_

#include "GeneralForce.h"

/**
 * @brief  Esta clase implementa la aplicación de una velocidad constante a las partículas.
 *
 * Básicamente lo que hace es cambiar la posición de la partícula de aucerdo a un vector de velocidad que se la pasa como parámetro
 */
class ConstantVelocity : public GeneralForce {

public:
    ConstantVelocity(VectorND vel);

    ConstantVelocity(VectorND vel, std::vector<int> toParts);

    virtual ~ConstantVelocity();

    virtual void applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation *sim);

private:
    VectorND velocity;
};

#endif /* FORCES_CONSTANTVELOCITY_H_ */
