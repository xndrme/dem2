/*
 * GeneralForce.cpp
 *
 *  Created on: Mar 4, 2015
 *      Author: alvarojavier
 */

#include "GeneralForce.h"
#include <core/ParticleContainer.h>

GeneralForce::GeneralForce() {
}

GeneralForce::GeneralForce(std::vector<int> toParticles) : toParticles(toParticles) {
}

GeneralForce::~GeneralForce() {
}

void GeneralForce::beforeIntegration(Simulation* sim) {
	int j;
	ParticleContainer& parts = sim->getParticles();
	if(toParticles.size()>0) {
		for(int i=0;i<toParticles.size();i++) {
			j = toParticles[i];
			applyForce(j,parts[j],sim);
		}
	} else {
		for(j=0;j<parts.size();j++)
			applyForce(j,parts[j],sim);
	}
}
