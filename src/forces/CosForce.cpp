//
// Created by Alvaro Javier on 6/12/2015.
//

#include "CosForce.h"


void CosForce::applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation *sim) {
    VectorND force = VectorND(
        a.x()*cos(fmod(b.x()*sim->getTime(),M_2_PI)),
        a.y()*cos(fmod(b.y()*sim->getTime(),M_2_PI)),
        a.z()*cos(fmod(b.z()*sim->getTime(),M_2_PI))
    );
    sim->getForces().addForce(partIndex,force);
}
