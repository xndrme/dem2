//
// Created by Alvaro Javier on 6/12/2015.
//

#include "CosVelocity.h"

void CosVelocity::applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation *sim) {
    part->setCenterBefore(part->getCenter());
    part->setCenter(
            part->getCenter() + VectorND(
                    a.x() * cos(fmod(b.x() * sim->getTime(), M_2_PI)),
                    a.y() * cos(fmod(b.y() * sim->getTime(), M_2_PI)),
                    a.z() * cos(fmod(b.z() * sim->getTime(), M_2_PI))
            )
    );
    part->setFixedPoint(
            part->getFixedPoint() + VectorND(
                    a.x() * cos(fmod(b.x() * sim->getTime(), M_2_PI)),
                    a.y() * cos(fmod(b.y() * sim->getTime(), M_2_PI)),
                    a.z() * cos(fmod(b.z() * sim->getTime(), M_2_PI))
            )
    );

}
