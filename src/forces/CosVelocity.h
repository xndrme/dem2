//
// Created by Alvaro Javier on 6/12/2015.
//

#ifndef DEM2_COSVELOCITY_H
#define DEM2_COSVELOCITY_H


#include "GeneralForce.h"
/**
 * @brief Esta clase modela una Velocidad que se le aplica a las partículas utilizando la funcion coseno.
 *
 * La formula es en cada instante de tiempo t, v=a*cos(bt), donde las operaciones se aplican para cada componente del
 * los vectores a y b.
 */
class CosVelocity : public GeneralForce {

public:

    /**
     * Constructor que recibe los vectores a y b como parámetros para la fórmula
     * v=a*cos(bt)
     *
     * @param a: vector de valores para el coeficiente a
     * @param b: vector de valores para el coeficiente b
     */
    CosVelocity(VectorND a, VectorND b) : a(a), b(b) { }

    /**
     * Este constructor ademas de los vectores a y b, espera como parámetro una lista de IDs de partículas para aplicar
     * la velocidad.
     *
     * @parmam toParts: lista de IDs de las partículas a las cuales aplicar la velocidad
     * @param a: vector de valores para el coeficiente a
     * @param b: vector de valores para el coeficiente b
     */
    CosVelocity(VectorND a, VectorND b, std::vector<int> toParts) : GeneralForce(toParts), a(a), b(b) { }

    virtual ~CosVelocity() { }

    virtual void applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation *sim);

private:
    ///parámetro para el cálculo de la velocidad: v=a*cos(bt)
    VectorND a, b;

};


#endif //DEM2_COSVELOCITY_H
