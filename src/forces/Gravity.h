/*
 * File:   GravityEngine.h
 * Author: soad
 *
 * Created on June 25, 2011, 1:06 PM
 */

#ifndef GRAVITY_H
#define	GRAVITY_H

#include <core/GeneralPurposeEngine.h>

/**
 * @brief Esta clase modela la fuerza de gravedad.
 */
class Gravity: public GeneralPurposeEngine {

	VectorND G;

public:
	Gravity(VectorND gravity);
	virtual void beforeIntegration(Simulation* sim);
	virtual ~Gravity();
};

#endif	/* GRAVITY_H */

