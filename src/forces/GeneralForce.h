/*
 * GeneralForce.h
 *
 *  Created on: Mar 4, 2015
 *      Author: alvarojavier
 */

#ifndef GENERALFORCE_H_
#define GENERALFORCE_H_

#include <vector>
#include <core/GeneralPurposeEngine.h>
#include "boost/shared_ptr.hpp"

/**
 * @brief Esta clase es base para cualquier clase que aplique una fuerza de forma general a todas las partículas o a un
 * subconjunto de ellas.
 *
 * Se puede utilizar para actualizar otras propiedades también como puede ser la posición de las
 * partículas, así se puede implementar una velocidad constante.
 */
class GeneralForce : public GeneralPurposeEngine{
public:
	/**
	 * Con este constructor la acción de las clases herederas de esta se aplicará a TODAS las partículas
	 */
	GeneralForce();
	/**
	 * Con este constructor la acción de las clases herederas de esta se aplicará a las partículas cuyo id esté en la
	 * lista.
	 *
	 * @param toParticles: lista de IDs de partículas a las cuales se le va a aplicar la acción de la clase.
	 */
	GeneralForce(std::vector<int> toParticles);
	virtual ~GeneralForce();

	/**
	 * En este método se debe implementar la acción específica de la clase sobre las partículas.
	 *
	 * @param partIndex: índice de la partícula a la cual se le aplica la acción
	 * @param part: puntero a la partícula
	 * @para sim: puntero a la simulación global
	 */
	virtual void applyForce(int partIndex, boost::shared_ptr<Particle> part, Simulation* sim) = 0;

	/**
	 * Este método implementa la mecánica de aplicación de acciones a las partículas según la lista de IDs.
	 */
	virtual void beforeIntegration(Simulation* sim);
private:
	///Lista de IDs de partículas a las cuales se le va a aplicar la acción de la clase.
	std::vector<int> toParticles;
};

#endif /* GENERALFORCE_H_ */
