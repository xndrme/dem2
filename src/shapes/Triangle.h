/*
 * Triangle.h
 *
 *  Created on: 15/04/2015
 *      Author: xndrme
 */

#ifndef SHAPES_TRIANGLE_H_
#define SHAPES_TRIANGLE_H_

#include <core/Shape.h>
#include <core/MathDefs.h>

/**
 * @brief Implementación de Forma de Triángulo para las partículas
 */
class Triangle: public Shape {
public:
	Triangle();

	virtual ~Triangle();
	AABB getAABB(VectorND center);

	const VectorND& getV1() const {
		return v1;
	}

	void setV1(const VectorND& v1) {
		this->v1 = v1;
	}

	const VectorND& getV2() const {
		return v2;
	}

	void setV2(const VectorND& v2) {
		this->v2 = v2;
	}

	const VectorND& getV0() const {
		return v0;
	}

	void setV0(const VectorND& v0) {
		this->v0 = v0;
	}

private:
	///Vectores que definen los tres puntos del triángulo partiendo del centro!
	VectorND v0,v1,v2;
};

#endif /* SHAPES_TRIANGLE_H_ */
