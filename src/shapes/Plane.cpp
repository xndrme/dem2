/* 
 * File:   Plane.cpp
 * Author: soad
 * 
 * Created on June 6, 2011, 2:52 PM
 */

#include <stdexcept>

#include "Plane.h"

Plane::Plane() :
		normal(0,1,0) {
}

AABB Plane::getAABB(VectorND center) {
	throw std::logic_error("No AABB for a plane!");
}

Plane::Plane(const Plane& orig) {
	normal = orig.normal;
}

Plane::~Plane() {
}

