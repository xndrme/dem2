/*
 * File:   Plane.h
 * Author: soad
 *
 * Created on June 6, 2011, 2:52 PM
 */

#ifndef PLANE_H
#define	PLANE_H

#include <core/MathDefs.h>
#include <core/Shape.h>

/**
 * @brief Modela la forma de Plano para las partículas (OJO INCOMPLETA!)
 */
class Plane : public Shape {
	VectorND normal;
	public:
	Plane();
	Plane(const Plane& orig);
	virtual AABB getAABB(VectorND center);
	virtual ~Plane();

	VectorND getNormal() const {
		return normal;
	}

	void setNormal(const VectorND& normal) {
		this->normal = normal.normalized();
	}
};

#endif	/* PLANE_H */

