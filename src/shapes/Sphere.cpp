/* 
 * File:   Sphere.cpp
 * Author: soad
 * 
 * Created on June 6, 2011, 11:04 AM
 */

#include "Sphere.h"

Sphere::Sphere() :
		radius(0) {
}

Sphere::Sphere(Real rad) :
		radius(rad) {
}

AABB Sphere::getAABB(VectorND center) {
	return AABB(center - VectorND::Constant(radius), center + VectorND::Constant(radius));
}

Sphere::Sphere(const Sphere& orig) {
	radius = orig.radius;
}

Sphere::~Sphere() {
}

