/*
 * Triangle.cpp
 *
 *  Created on: 15/04/2015
 *      Author: xndrme
 */

#include "Triangle.h"

Triangle::Triangle() {}

Triangle::~Triangle() {}

AABB Triangle::getAABB(VectorND center) {
	VectorND P0 = center+v0, P1 = center+v1, P2 = center+v2;
	VectorND Xs(P0.x(),P1.x(),P2.x()),Ys(P0.y(),P1.y(),P2.y()),Zs(P0.z(),P1.z(),P2.z());
	VectorND MAX(Xs.maxCoeff(),Ys.maxCoeff(),Zs.maxCoeff());
	VectorND MIN(Xs.minCoeff(),Ys.minCoeff(),Zs.minCoeff());
	return AABB(MIN,MAX);
}


