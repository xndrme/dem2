/*
 * File:   Sphere.h
 * Author: soad
 *
 * Created on June 6, 2011, 11:04 AM
 */

#ifndef Sphere_H
#define	Sphere_H

#include <core/Shape.h>
#include <core/MathDefs.h>

/**
 * @brief Modela la forma de esfera para las partículas.
 */
class Sphere: public Shape {
	Real radius; ///radio de la esfera

public:
	Sphere();
	Sphere(Real rad);
	Sphere(const Sphere& orig);
	virtual AABB getAABB(VectorND center);
	virtual ~Sphere();

	void setRadius(Real radius) {
		this->radius = radius;
	}

	Real getRadius() const {
		return radius;
	}

};

#endif	/* Sphere_H */

