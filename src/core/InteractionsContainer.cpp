/*
 * File:   InteractionsContainer.cpp
 * Author: soad
 *
 * Created on July 4, 2011, 3:48 PM
 */

#include <map>

#include "InteractionsContainer.h"
#include <boost/foreach.hpp>
#include <list>

using boost::shared_ptr;
using namespace std;

bool InteractionsContainer::insert(const shared_ptr<Interaction>& i) {

	Particle::id_t id1 = i->getId1(), id2 = i->getId2();
	if (id1 > id2)
		swap(id1, id2);

	if ((size_t) id1 >= vecmap.size())
		vecmap.resize(id1 + 1);

	//false si ya existia esta interaccion
	if (!vecmap[id1].insert(pair<Particle::id_t, size_t>(id2, currentSize)).second)
		return false;

	interactions.resize(++currentSize);
	interactions[currentSize - 1] = i;

	return true;
}

bool InteractionsContainer::insert(Particle::id_t id1, Particle::id_t id2) {
	shared_ptr<Interaction> i(new Interaction(id1, id2));
	return insert(i);
}

void InteractionsContainer::clear() {

	vecmap.clear();
	interactions.clear();
	//pendingErase.clear();
	currentSize = 0;
}

bool InteractionsContainer::erase(Particle::id_t id1, Particle::id_t id2) {
	if (id1 > id2)
		swap(id1, id2);

	if ((size_t) id1 >= vecmap.size())
		return false; // id1 fuera de limites

	map<Particle::id_t, size_t>::iterator fii = vecmap[id1].find(id2);

	if (fii == vecmap[id1].end())
		return false; //no existe la interaccion id1 <-> id2

	size_t iid = (*fii).second;
	vecmap[id1].erase(fii);

	//si la interacción eliminada no es la última
	if (iid < currentSize - 1) { //mover la última al lugar de la que estamos eliminando
		interactions[iid] = interactions[currentSize - 1];

		// ajustar vecmap
		id1 = interactions[iid]->getId1();
		id2 = interactions[iid]->getId2();
		if (id1 > id2)
			swap(id1, id2);
		vecmap[id1][id2] = iid;
	}
	//ahora se puede eliminar la ultima interacción
	interactions.resize(--currentSize);

	return true;
}

boost::shared_ptr<Interaction>& InteractionsContainer::find(Particle::id_t id1, Particle::id_t id2) {
	if (id1 > id2)
		swap(id1, id2);

	if ((size_t) id1 >= vecmap.size()) {
		return empty;
	}

	map<Particle::id_t, size_t>::iterator fii = vecmap[id1].find(id2);
	if (fii != vecmap[id1].end())
		return interactions[(*fii).second];
	else {
		return empty;
	}

}

void InteractionsContainer::eraseNonReal() {
	typedef pair<int, int> Ids;
	list<Ids> ids;

	BOOST_FOREACH(const shared_ptr<Interaction>& i, *this ) {
		if (!i->isReal())
			ids.push_back(Ids(i->getId1(), i->getId2()));
	}

	BOOST_FOREACH(const Ids& id, ids) {
		this->erase(id.first, id.second);
	}
}

void InteractionsContainer::requestErase(Particle::id_t id1, Particle::id_t id2) {
	find(id1, id2)->reset();
	PendingErase p = make_pair(id1, id2);
	pendingsToErase.push_back(p);
}

