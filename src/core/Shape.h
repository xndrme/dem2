/*
 * File:   Shape.h
 * Author: soad
 *
 * Created on June 6, 2011, 10:39 AM
 */

#ifndef SHAPE_H
#define	SHAPE_H

#include "AABB.h"

/**
 * @brief Clase base para la forma de las partículas
 */
class Shape {
public:
	/**
	 * Devuelve la caja acotadora de esta partícula calculada según un centro, que normalmente debe ser el centro de una
	 * partícual con esta Forma.
	 */
	virtual AABB getAABB(VectorND center)=0;
	virtual ~Shape();
};

#endif	/* SHAPE_H */

