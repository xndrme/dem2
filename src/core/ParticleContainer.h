/*
 * File:   ParticleContainer.h
 * Author: soad
 *
 * Created on June 27, 2011, 10:20 AM
 */

#ifndef PARTICLECONTAINER_H
#define	PARTICLECONTAINER_H

#include "Particle.h"
#include <vector>
#include <boost/shared_ptr.hpp>

/**
 * @brief  Contenedor que almacea todas las partículas de la simulación.
 *
 * Realiza operaciones sobre le conjunto de partículas de una forma centralizada.
 */
class ParticleContainer {
	///Colección de las partículas
	typedef std::vector<boost::shared_ptr<Particle> > ParticleCollectionT;

	/// contenedor interno de las partículas
	ParticleCollectionT particles;
	/// primer índice libre
	unsigned int firstFree;

public:

	typedef ParticleCollectionT::iterator IteratorT;
	typedef ParticleCollectionT::const_iterator ConstIteratorT;

	ParticleContainer() : firstFree(0u) {
	}

	/**
	 * Inserta una partícula en el contenedor
	 */
	unsigned int insert(boost::shared_ptr<Particle>&);
	/**
	 * Inserta una partícula en la posición indicada
	 */
	unsigned int insert(boost::shared_ptr<Particle>& b, unsigned int id);

	/**
	 * Limpia el contenedor de partículas
	 */
	void clear() {
		particles.clear();
		firstFree = 0;
	}

	IteratorT begin() {
		return particles.begin();
	}
	IteratorT end() {
		return particles.end();
	}
	ConstIteratorT begin() const {
		return particles.begin();
	}
	ConstIteratorT end() const {
		return particles.end();
	}
	/**
	 * Tamaño del contenedor
	 */
	unsigned int size() const {
		return particles.size();
	}

	boost::shared_ptr<Particle>& operator[](unsigned int id) { return particles[id]; }
	const boost::shared_ptr<Particle>& operator[](unsigned int id) const { return particles[id]; }

	/**
	 * Verifica si existe una partícula con el índice indicado por el id
	 */
	bool exists(unsigned int id) const {
		return ((size_t) id < particles.size()) && ((bool) particles[id]);
	}

	/**
	 * Elimina una partícula del contenedor dado su id
	 */
	bool erase(unsigned int id) {
		if (!exists(id))
			return false;

		firstFree = std::min(firstFree, id);
		particles[id] = boost::shared_ptr<Particle>();
		return true;
	}

private:
	/**
	 * Busca un id libre dentro del contenedor (un índice vacío)
	 */
	unsigned int findFreeId();

};

#endif	/* PARTICLECONTAINER_H */

