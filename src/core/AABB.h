/*
 * File:   AABB.h
 * Author: soad
 *
 * Created on August 8, 2011, 12:58 AM
 */

#ifndef AABB_H
#define	AABB_H

#include "MathDefs.h"
/**
 * @brief Caja acotadora para las partículas
 */
class AABB {
	/**
	 * Esquinas inferior-izquierda y superior-derecha que contienen completamente un cuerpo.
	 */
	VectorND min, max;
	public:
	AABB(VectorND min, VectorND max) :
			min(min), max(max) {
	}

	VectorND getMax() const {
		return max;
	}

	VectorND getMin() const {
		return min;
	}

};

#endif	/* AABB_H */

