/*
 * File:   Simulation.cpp
 * Author: soad
 *
 * Created on April 27, 2011, 10:39 AM
 */

#include "Simulation.h"
#include "Interaction.h"
#include <iostream>
#include <boost/foreach.hpp>
#include <integrators/ForcesResetter.h>

using boost::shared_ptr;

Simulation::Simulation() : simulationBounds(AABB(VectorND::Zero(),VectorND::Zero())) {

	cohesiveDistance = 0.0655597276608;

	time = 0;
	nSteps = 0;
	timeStep = 0;
	iter = 0;

	forceResetterEngine = shared_ptr<Engine>(new ForcesResetter());
	integrationEngine = shared_ptr<Engine>();
	collisionEngine = shared_ptr<Engine>();

	forceDamping = 0;
    torqueDamping = 0;
    applyDamping = false;
    viscous = false;

}

Simulation::~Simulation() {
}

void Simulation::run() {

	//inicializar las engines
	BOOST_FOREACH(const shared_ptr<GeneralPurposeEngine>& engine, generalPurposeEngines) {
		engine->init(this);
	}

	for (unsigned int i = 0; i < nSteps; i++) {

		//llamar a las engines antes de efectuar un paso
		BOOST_FOREACH(const shared_ptr<GeneralPurposeEngine>& engine, generalPurposeEngines) {
			engine->beforeStep(this);
		}

		//std::cout << iter + 1 << ":" << std::endl;
		step();

		time += timeStep;
		iter++;
	}

}

void Simulation::doDamping() {
    VectorND fD,tD;
    boost::shared_ptr<Particle> p;
    for(unsigned int i=0;i<particles.size();++i) {
        p = particles[i];
        if(p->isMobile()) {
            if(viscous) {
                fD = -forceDamping* p->getMass() *p->getVelocity();
                tD = (-torqueDamping * p->getInertiaMoment()* p->getAngularVelocity()).transpose();
            } else {
                if(p->getVelocity().norm() > MathDefs::EPSILON) {
                    fD = -forceDamping * (forces.getForce(i).norm()) * (p->getVelocity().normalized());
                } else {
                    fD = VectorND::Zero();
                }
                if(p->getAngularVelocity().norm() > MathDefs::EPSILON) {
                    tD = -torqueDamping * (forces.getTorque(i).norm()) * (p->getAngularVelocity().normalized());
                } else {
                    tD = VectorND::Zero();
                };
            }
            forces.addForce(i,fD);
            forces.addTorque(i,tD);
        }
    }
}

void Simulation::step() {

	//Reset todas la  fuerzas y momentos
	forceResetterEngine->execute(this);

	//llamar engines antes de colisiones
	BOOST_FOREACH(const shared_ptr<GeneralPurposeEngine>& engine, generalPurposeEngines) {
		engine->beforeCollitions(this);
	}

	//Busqueda de vecindad
	collisionEngine->execute(this);

	//llamar a las engines antes de las interacciones
	BOOST_FOREACH(const shared_ptr<GeneralPurposeEngine>& engine, generalPurposeEngines) {
		engine->beforeInteractions(this);
	}

	//Calcular fuerzas de las interacciones

	shared_ptr<Interaction> inter;//iterador para la interacciones
	int matId1,matId2; //materiales
	int partIdModel; //id de la partícula que define el modelo para esta interacción
	shared_ptr<ModelEngine> model; //modelo para la interacción
	Interface interface; //interfaz en esta itneracción
	for (unsigned int i = 0; i < interactions.size(); i++) {
		 inter = interactions[i];

		 //obtener los materiales de la partículas
		 matId1 = particles[inter->getId1()]->getMaterialId();
		 matId2 = particles[inter->getId2()]->getMaterialId();

		 partIdModel = inter->getId1();
		 if(matId1!=matId2) {
			 //buscar la interfaz
			 interface =  interfaces[matId1][matId2];
			 //obtener el modelo
			 model = models[interface.modelId];
			 //obtener cual es la partícula que define el modelo de esta interacción
			 partIdModel = (materials[matId1]->modelId==interface.modelId)? inter->getId1() : inter->getId2();
		 } else {
			 //obtener el modelo
			 model = models[materials[matId1]->modelId];
		 }

		 //ejecutar el modelo, si los materiales son distintos se corre en modo interfaz
		 model->execute(inter,this,matId1!=matId2,interface,partIdModel);
	}
	interactions.erasePending(this,this);

    //efectuar el damping de la simulación
    if(applyDamping) doDamping();

    //llamar a las engines antes de integrar
	BOOST_FOREACH(const shared_ptr<GeneralPurposeEngine>& engine, generalPurposeEngines) {
		engine->beforeIntegration(this);
	}

    //Integrar
	integrationEngine->execute(this);

}


bool Simulation::shouldBeErased(Particle::id_t id1, Particle::id_t id2, Simulation *sim) {
	int matId1 = particles[id1]->getMaterialId();
	int matId2 = particles[id2]->getMaterialId();
	boost::shared_ptr<ModelEngine> model; //modelo para la interacción
	if(matId1!=matId2) {
		//buscar la interfaz
		Interface interface =  interfaces[matId1][matId2];
		//obtener el modelo
		model = models[interface.modelId];
	} else {
		//obtener el modelo
		model = models[materials[matId1]->modelId];
	}
	return model->shouldBeErased(id1,id2,this);
	//return true;
}
