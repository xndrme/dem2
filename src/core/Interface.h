/*
 * Interface.h
 *
 *  Created on: 01/05/2015
 *      Author: xndrme
 */

#ifndef CORE_INTERFACE_H_
#define CORE_INTERFACE_H_

#include "MathDefs.h"

/**
 * @brief Guarda información relativa a las interfaces entre los materiales de la simulación.
 *
 * Aquí se define cuando dos materiales específicos entran en contacto, cuál modelo se debe usar.
 */
class Interface {
public:
	/**
	 * Constructor por defecto
	 */
	Interface() : modelId(0), rn(.0), rt(.0) {}
	/**
	 * Este constructor recibe el modelo que se debe usar para esta interfaz y los valores de
	 * resistencia a compresión y tracción específicos para esta interfaz.
	 */
	Interface(int modelId, Real rn, Real rt) : modelId(modelId), rn(rn), rt(rt) {}
	~Interface() {}

	///Id del modelo a utilizar en esta interfaz entre materiales
	int modelId;

	Real rn,rt;
};

#endif /* CORE_INTERFACE_H_ */
