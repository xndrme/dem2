/* 
 * File:   Dispatcher.cpp
 * Author: soad
 * 
 * Created on July 1, 2011, 12:59 PM
 */

#include "Dispatcher.h"
#include <shapes/Sphere.h>
#include <shapes/Plane.h>
#include <shapes/Triangle.h>
#include <contacts/GeomSphereSphere.h>
#include <contacts/GeomSpherePlane.h>
#include <contacts/GeomTriangleSphere.h>

using boost::shared_ptr;

shared_ptr<GeometryFunctor> GeometryDispatcher::dispatch(Shape* first, Shape* second, bool& swap, bool& existHandler) {
	swap = false;
	existHandler = false;
	shared_ptr<GeometryFunctor> handler = shared_ptr<GeometryFunctor>();

	//verificar si es esfera con esfera
	if ((bool) dynamic_cast<Sphere*>(first)	&& (bool) dynamic_cast<Sphere*>(second)) {
		handler = shared_ptr<GeometryFunctor>(new GeomSphereSphere());
		existHandler = true;
	} else

	//verficiar si es esfera con plano
	if ((bool) dynamic_cast<Sphere*>(first)	&& (bool) dynamic_cast<Plane*>(second)) {
		handler = shared_ptr<GeometryFunctor>(new GeomSpherePlane());
		existHandler = true;
	} else
	//verficiar si es plano con esfera
	if ((bool) dynamic_cast<Plane*>(first) && (bool) dynamic_cast<Sphere*>(second)) {
		swap = true;
		handler = shared_ptr<GeometryFunctor>(new GeomSpherePlane());
		existHandler = true;
	} else

	//verficiar si es triangulo con esfera
	if ((bool) dynamic_cast<Triangle*>(first)	&& (bool) dynamic_cast<Sphere*>(second)) {
		handler = shared_ptr<GeometryFunctor>(new GeomTriangleSphere());
		existHandler = true;
	} else
	//verficiar si es esfera con triangulo
	if ((bool) dynamic_cast<Sphere*>(first) && (bool) dynamic_cast<Triangle*>(second)) {
		swap = true;
		handler = shared_ptr<GeometryFunctor>(new GeomTriangleSphere());
		existHandler = true;
	}

	return handler;
}

GeometryFunctor::~GeometryFunctor() {
}

