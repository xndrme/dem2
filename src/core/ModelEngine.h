/*
 * ModelEngine.h
 *
 *  Created on: 01/05/2015
 *      Author: xndrme
 */

#ifndef CORE_MODELENGINE_H_
#define CORE_MODELENGINE_H_

#include "Simulation.h"
#include "Interaction.h"
#include "Interface.h"
#include "Dispatcher.h"
#include "Material.h"

#include "boost/shared_ptr.hpp"

class Simulation;

/**
 * @brief Esta clase es para definir Egines que funcionen como modelos de interacción.
 */
class ModelEngine {
protected:
    ///Determina la geometría de contacto para cada interacción
	GeometryDispatcher geomDispacher;
public:
	ModelEngine();
	virtual ~ModelEngine();

	/**
	 * Aquí se ejecuta el cálculo de fuerzas de las interacciones
	 *
	 * @param inter la interacción a la cual se le van a calcular las fuerzas
	 * @param sim puntero al objeto de la simulación global
	 * @param isInterface este booleano indica si el modelo debe funcionar en modo interfaz (cuando los mareriales de
	 * las partículas son distintos)
	 * @param interface objeto que indica los datos para esta interfaz en caso que isInterface sea <code>true</code>
	 * @param partIdModel ID de la partícula cuyo modelo se usa como interfaz (solo para cuando isInterface sea
	 * <code>true</code>)
	 *
	 */
	virtual void execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel)=0;

    /**
     * Devuelve la instancia de material que es capaz de manejar este modelo. Así se pueden implementar materiales propios
     * que utlicen propiedas además de las genéricas knt,knc,kt,kn, etc.
     */
	virtual Material* getMaterialInstance() { return new Material(); }

	/**
	 * Predicado para borrar las interecciones marcadas
	 * Nota: por ahora siempre elimina las interacciones marcadas para borrar,
	 * es decir siempre devuelve true.
	 **/
	virtual bool shouldBeErased(Particle::id_t id1, Particle::id_t id2, Simulation *sim) {
		return true;
	}
};

#endif /* CORE_MODELENGINE_H_ */
