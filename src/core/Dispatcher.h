/*
 * File:   Dispatcher.h
 * Author: soad
 *
 * Created on July 1, 2011, 12:59 PM
 */

#ifndef DISPATCHER_H
#define	DISPATCHER_H

#include "Shape.h"
#include "Particle.h"
#include "ContactGeometry.h"

/**
 * @brief Clase base para los functores geométricos que manejan las geometrías de contacto
 */

class GeometryFunctor {
public:
	/**
	 * Aquí se ejecuta la acción del functor de contacto entre las geometrías
	 * Su tarea para instancia es inicalizar la geometría de contacto y devolver
	 * un valor que indique si hay interacción a no
	 */
	virtual bool go(const boost::shared_ptr<Particle>& p1, const boost::shared_ptr<Particle>& p2, boost::shared_ptr<ContactGeometry>& geom)=0;

	GeometryFunctor() {};
	virtual ~GeometryFunctor();
};

/**
 * @brief Es la encargada de depachar el tipo de gemoetría de contacto necesaria dada dos partículas específicas
 */

class GeometryDispatcher {
public:
	boost::shared_ptr<GeometryFunctor> dispatch(Shape* first, Shape* second, bool& swap, bool& existHandler);
};

#endif	/* DISPATCHER_H */

