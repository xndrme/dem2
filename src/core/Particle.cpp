/*
 * File:   Circle.cpp
 * Author: soad
 *
 * Created on April 27, 2011, 10:15 AM
 */

#include "Particle.h"

const Particle::id_t Particle::ID_NONE = -1;

Particle::Particle() :
		id(Particle::ID_NONE),
		center(VectorND::Zero()),
		centerBefore(VectorND::Zero()),
		mass(0.0),
		mobile(true),
		fixedPoint(VectorND::Zero()),
		velocity(VectorND::Zero()),
		acceleration(VectorND::Zero()),
		rotationalAngle(VectorND::Zero()),
		angularVelocity(VectorND::Zero()),
		angularAcceleration(VectorND::Zero()),
		inertiaMoment(MatrixNN::Zero()),
		inverseInertiaMoment(MatrixNN::Zero()) {
}

Particle::Particle(VectorND cent) :
		id(Particle::ID_NONE),
		center(cent),
		centerBefore(cent),
		mass(0.0),
		mobile(true),
		fixedPoint(VectorND::Zero()),
		velocity(VectorND::Zero()),
		acceleration(VectorND::Zero()),
		rotationalAngle(VectorND::Zero()),
		angularVelocity(VectorND::Zero()),
		angularAcceleration(VectorND::Zero()),
		inertiaMoment(MatrixNN::Zero()),
		inverseInertiaMoment(MatrixNN::Zero()) {
}

Particle::Particle(const Particle& orig) :
		id(orig.id),
		center(orig.center),
		centerBefore(orig.centerBefore),
		mass(orig.mass),
		mobile(orig.mobile),
		fixedPoint(orig.fixedPoint),
		velocity(orig.velocity),
		acceleration(orig.acceleration),
		rotationalAngle(orig.rotationalAngle),
		angularVelocity(orig.angularVelocity),
		angularAcceleration(orig.angularAcceleration),
		inertiaMoment(orig.inertiaMoment),
		inverseInertiaMoment(orig.inverseInertiaMoment) {
}

Particle::~Particle() {
}

std::ostream& operator<<(std::ostream& os, const Particle& p) {
	// char fillc = os.fill('0');
	//setw no mq quizo pinchar aki
	os << "Centro: " << p.getCenter() << " " << "PuntoFijo: "
			<< p.getFixedPoint() << " " << "VelocidadAngular: "
			<< p.getAngularVelocity() << " " << "Velocidad: "
			<< p.getVelocity();
	return os;
}

