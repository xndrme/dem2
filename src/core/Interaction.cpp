/*
 * File:   Interaction.cpp
 * Author: soad
 *
 * Created on June 18, 2011, 11:32 AM
 */

#include "Interaction.h"
using boost::shared_ptr;

Interaction::Interaction(Particle::id_t id_1, Particle::id_t id_2, bool isCohesive) :
		id1(id_1), id2(id_2), cohesive(isCohesive), predTangForce(VectorND::Zero()), springPoint1(VectorND::Zero()),
		springPoint2(VectorND::Zero()), previousURN(0), previousURT(0) {}

Interaction::~Interaction() {}

