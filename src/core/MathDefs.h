/*
 * File:   Math.h
 * Author: soad
 *
 * Created on May 21, 2011, 2:40 PM
 */

#ifndef MATHDEFS_H
#define	MATHDEFS_H

#ifdef __MINGW_H //Esto no pincha bien con MINGW en windows
#  define EIGEN_DONT_VECTORIZE
#  define EIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT
#endif

#include <Eigen/Dense>
#include <limits>
#include <float.h>
#include <cstdlib>
#include <math.h>

typedef double Real;

typedef Eigen::Matrix<Real, 2, 1> Vector2D;
typedef Eigen::Matrix<Real, 3, 1> Vector3D;
typedef Eigen::Matrix<Real, 2, 2> Matrix22;
typedef Eigen::Matrix<Real, 3, 3> Matrix33;

typedef Vector3D VectorND;
typedef Matrix33 MatrixNN;

namespace MathDefs {

	const Real EPSILON = DBL_EPSILON;
	Real sign(Real x);
}


#endif	/* MATHDEFS_H */

