/*
 * File:   ContactGeometry.h
 * Author: soad
 *
 * Created on June 30, 2011, 1:27 PM
 */

#ifndef CONTACTGEOMETRY_H
#define	CONTACTGEOMETRY_H

#include "MathDefs.h"

/**
 * @brief  Datos de la geometría del contacto (cuando existe) entre dos partículas
 */

class ContactGeometry {

	///punto de contacto de referencia entre las partículas
	VectorND contactPoint;
	///normal al plano de contacto entre las partículas
	VectorND normal;
	///valor que representa la penetración entre las partículas
	Real uRN;
	///indica si se están acercando las partículas
	bool approaching;

public:
	ContactGeometry() :
		contactPoint(VectorND::Zero()), normal(VectorND::Zero()), uRN(0), approaching(false) {}

	virtual ~ContactGeometry();

	void setURN(Real uRN) {
		this->uRN = uRN;
	}
	Real getURN() const {
		return uRN;
	}
	void setNormal(VectorND normal) {
		this->normal = normal;
	}
	VectorND getNormal() const {
		return normal;
	}
	void setContactPoint(VectorND contactPoint) {
		this->contactPoint = contactPoint;
	}
	VectorND getContactPoint() const {
		return contactPoint;
	}
	void setApproaching(bool approaching) {
		this->approaching = approaching;
	}
	bool isApproaching() const {
		return approaching;
	}
};

#endif	/* CONTACTGEOMETRY_H */

