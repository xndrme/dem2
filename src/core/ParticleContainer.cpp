/*
 * File:   ParticleContainer.cpp
 * Author: soad
 *
 * Created on June 27, 2011, 10:20 AM
 */

#include "ParticleContainer.h"

using boost::shared_ptr;

unsigned int ParticleContainer::findFreeId() {
	unsigned int max = particles.size();

	for (; firstFree < max; firstFree++) {
		if (!(bool) particles[firstFree])
			return firstFree;
	}

	return particles.size();
}

unsigned int ParticleContainer::insert(shared_ptr<Particle>& p) {
	Particle::id_t newId = findFreeId();
	return insert(p, newId);
}

unsigned int ParticleContainer::insert(shared_ptr<Particle>& p, unsigned int id) {
	assert(id >= 0);
	if ((size_t) id >= particles.size())
		particles.resize(id + 1);
	p->id = id;
	particles[id] = p;
	return id;
}

