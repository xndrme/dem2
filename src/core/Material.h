/*
 * Material.h
 *
 *  Created on: 01/05/2015
 *      Author: xndrme
 */

#ifndef CORE_MATERIAL_H_
#define CORE_MATERIAL_H_

#include <boost/shared_ptr.hpp>
#include "MathDefs.h"
#include <stdlib.h>

/**
 * @brief Esta clase almacena información relativa al material de una partícula
 */
class Material {
public:
	/**
	 * Constructor.
	 * Recibe como parámetro el id del modelo y las constantes espercíficas de este material.
	 */
	Material() : id(++lastId),modelId(0),knc(0),knt(0),kt(0),rn(0),rt(0),coulomb(0) {}
	virtual ~Material() {}

	///Id de este material
	int id;
	///Id del modelo que maneja las interacciones entre partículas de este material.
	int modelId;
	///Constantes propias del material
	Real knc,knt,kt,rn,rt,coulomb;

	/**
	 * Esta función se usa para establecer propiedades de materiales específicos que hereden de esta clase.
	 * Con este mecanismo el CsvLoader puede establecer las propiedades del material sin conocer la clase propia
	 * de la implementación.
	 * Los nuevos materiales que necesiten propieades específicas debem implemetar este método y guardar la información
	 * que reciban del Loader.
	 */
	virtual void setProperty(std::string name, Real value) {}

	///esto es para crear los id automáticamente, solo manipular para resetear el contador
	static int lastId;
};
#endif /* CORE_MATERIAL_H_ */
