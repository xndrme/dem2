/*
 * File:   Simulation.h
 * Author: soad
 *
 * Created on April 27, 2011, 10:39 AM
 */

#ifndef SIMULATION_H
#define	SIMULATION_H

#include "InteractionsContainer.h"
#include "Engine.h"
#include "ModelEngine.h"
#include "GeneralPurposeEngine.h"
#include "MathDefs.h"
#include "Particle.h"
#include "Interaction.h"
#include "ForceContainer.h"
#include "ParticleContainer.h"
#include "Material.h"
#include "Interface.h"
#include <vector>
#include <boost/shared_ptr.hpp>

class Engine;
class ModelEngine;
class GeneralPurposeEngine;

/**
 * @brief Representa una simulacion en su totalidad
 */
class Simulation {
public:
	Simulation();

	virtual ~Simulation();

	/**
	 * Ejecuta la simulacion.
	 */
	void run();

	/**
	 * Este método llama a los repectivos métodos de los modelos de cada partíucula para saber si
	 * se deben borrar las interacciones.
	 * Nota: actualmento todods devuelven true.
	 */
	virtual bool shouldBeErased(Particle::id_t id1, Particle::id_t id2, Simulation *sim);

	Real getCohesiveDistance() const {
		return cohesiveDistance;
	}

	void setCohesiveDistance(Real cohesiveDistance) {
		this->cohesiveDistance = cohesiveDistance;
	}

	boost::shared_ptr<Engine>& getCollisionEngine() {
		return collisionEngine;
	}

	void setCollisionEngine(boost::shared_ptr<Engine> collisionEngine) {
		this->collisionEngine = collisionEngine;
	}

	boost::shared_ptr<Engine>& getForceResetterEngine() {
		return forceResetterEngine;
	}

	void setForceResetterEngine(boost::shared_ptr<Engine> forceResetterEngine) {
		this->forceResetterEngine = forceResetterEngine;
	}

	ForceContainer& getForces() {
		return forces;
	}

	std::vector<boost::shared_ptr<GeneralPurposeEngine> >& getGeneralPurposeEngines() {
		return generalPurposeEngines;
	}

	void setGeneralPurposeEngines(std::vector<boost::shared_ptr<GeneralPurposeEngine> > generalPurposeEngines) {
		this->generalPurposeEngines = generalPurposeEngines;
	}

	boost::shared_ptr<Engine>& getIntegrationEngine() {
		return integrationEngine;
	}

	void setIntegrationEngine(boost::shared_ptr<Engine> integrationEngine) {
		this->integrationEngine = integrationEngine;
	}

	InteractionsContainer& getInteractions() {
		return interactions;
	}

	unsigned int getIter() const {
		return iter;
	}

	void setIter(unsigned int iter) {
		this->iter = iter;
	}

	Real getMaximumRadius() const {
		return maximumRadius;
	}

	void setMaximumRadius(Real maximumRadius) {
		this->maximumRadius = maximumRadius;
	}

	Real getMinimumRadius() const {
		return minimumRadius;
	}

	void setMinimumRadius(Real minimumRadius) {
		this->minimumRadius = minimumRadius;
	}

	unsigned int getNsteps() const {
		return nSteps;
	}

	void setNSteps(unsigned int nSteps) {
		this->nSteps = nSteps;
	}

	ParticleContainer& getParticles() {
		return particles;
	}

	Real getRotationalDamping() const {
		return rotationalDamping;
	}

	void setRotationalDamping(Real rotationalDamping) {
		this->rotationalDamping = rotationalDamping;
	}

	AABB getSimulationBounds() const {
		return simulationBounds;
	}

	void setSimulationBounds(AABB simulationBounds) {
		this->simulationBounds = simulationBounds;
	}

	Real getTime() const {
		return time;
	}

	void setTime(Real time) {
		this->time = time;
	}

	Real getTimeStep() const {
		return timeStep;
	}

	void setTimeStep(Real timeStep) {
		this->timeStep = timeStep;
	}

	Real getTranslationalDamping() const {
		return translationalDamping;
	}

	void setTranslationalDamping(Real translationalDamping) {
		this->translationalDamping = translationalDamping;
	}

	std::vector<boost::shared_ptr<ModelEngine> >& getModels() {
		return models;
	}

	void setModels(const std::vector<boost::shared_ptr<ModelEngine> >& models) {
		this->models = models;
	}

	std::vector<boost::shared_ptr<Material> >& getMaterials() {
		return materials;
	}

	std::vector<std::vector<Interface> >& getInterfaces() {
		return interfaces;
	}

private:
	/**
	 * Efectuar un paso de la simulación
	 */
	void step();
    /**
     * Aplica el damping a la simulación
     */
    void doDamping();

	boost::shared_ptr<Engine> forceResetterEngine; ///Resetear todas la fuerzas al comienzo de la simulación.
	boost::shared_ptr<Engine> collisionEngine; ///Búsqueda de contactos
	boost::shared_ptr<Engine> integrationEngine; ///Integrar las fuerzas
	std::vector<boost::shared_ptr<GeneralPurposeEngine> > generalPurposeEngines; ///Engines genreales

	std::vector<boost::shared_ptr<ModelEngine> > models; ///listado de modelos para la simulación
	std::vector<boost::shared_ptr<Material> > materials; ///lista de materiales de las partículas
	std::vector<std::vector<Interface> > interfaces; ///listado de interfaces entre los modelos

	AABB simulationBounds; /// límites globales de la simulación

	Real translationalDamping;
	Real rotationalDamping;

	Real cohesiveDistance; ///cota máxima para la separación entre las patículas antes de que se rompa la cohesión

	Real time; /// tiempo actual de la simulación
	Real timeStep; /// paso de tiempo en cada iteración
	unsigned int nSteps; /// cantidad de pasos a efectuar
	unsigned int iter; ///iteración actual

	Real minimumRadius; /// cota máxima para los radios de las partículas
	Real maximumRadius; /// cota mínima para los radios de las partículas

	ParticleContainer particles; /// contenedor de partículas que intervienen en la simulación
	InteractionsContainer interactions; /// contenedor de interacciones que intervienen en cada paso
	ForceContainer forces; /// contenendor de fuerzas en cada paso

public:
    Real getForceDamping() const {
        return forceDamping;
    }

    void setForceDamping(Real forceDamping) {
        Simulation::forceDamping = forceDamping;
        if(forceDamping!=0.0 || torqueDamping!=0.0) {
            applyDamping = true;
        }
    }

    Real getTorqueDamping() const {
        return torqueDamping;
    }

    void setTorqueDamping(Real torqueDamping) {
        Simulation::torqueDamping = torqueDamping;
        if(forceDamping!=0.0 || torqueDamping!=0.0) {
            applyDamping = true;
        }
    }

    bool isApplyDamping() const {
        return applyDamping;
    }

    void setApplyDamping(bool applyDamping) {
        Simulation::applyDamping = applyDamping;
    }

    bool isViscous() const {
        return viscous;
    }

    void setViscous(bool viscous) {
        Simulation::viscous = viscous;
    }

private:
    Real forceDamping; ///constante de damping para las fuerzas
	Real torqueDamping; ///constante de damping para los torques
	bool applyDamping; ///para saber si se le aplica damping a esta simulacion o no
	bool viscous; ///indica si se usa un modelo de damping viscoso o no
};

#endif	/* SIMULATION_H */

