/*
 * File:   ForceContainer.h
 * Author: soad
 *
 * Created on June 26, 2011, 9:08 PM
 */

#ifndef FORCECONTAINER_H
#define	FORCECONTAINER_H

#include <vector>
#include "MathDefs.h"
#include "Particle.h"

/**
 * @brief Contenedor de fuerzas para las partículas
 */
class ForceContainer {

	/// lista de las fuerzar indexadas por partículas
	std::vector<VectorND> forces;
	std::vector<VectorND> torques;

	///tamaño de lalista de fuerzas
	size_t size;

public:

	ForceContainer() : size(0) {}

	/**
	 * Resetea todas fuerzas actuantes sobre la partículas a cero
	 */
	void reset() {
		memset(&forces[0], 0, sizeof(VectorND) * size);
		memset(&torques[0], 0, sizeof(VectorND::Zero()) * size);
	}

	/**
	 * Devuelve la fuerza que actúa sobre la partículas dado su id
	 */
	const VectorND& getForce(Particle::id_t id) {
		ensureSize(id);
		return forces[id];
	}

	/**
	 * Suma una fuerza a la fuerza total que actúa en la partículas
	 */
	void addForce(Particle::id_t id, const VectorND& f) {
		ensureSize(id);
		forces[id] += f;
	}

	const VectorND& getTorque(Particle::id_t id) {
		ensureSize(id);
		return torques[id];
	}

	void addTorque(Particle::id_t id, const VectorND& t) {
		ensureSize(id);
		torques[id] += t;
	}

private:

	/**
	 * Garantiza el tamaño del contenedor de fuerzas
	 */
	void ensureSize(Particle::id_t id) {
		if (size <= (size_t) id)
			resize(std::min((size_t) 1.5 * (id + 100), (size_t) (id + 2000)));
	}

	/**
	 * Cambia el tamaño del contenedor de fuerzas
	 */
	void resize(size_t newSize) {
		forces.resize(newSize, VectorND::Zero());
		torques.resize(newSize,VectorND::Zero());
		size = newSize;
	}

};

#endif	/* FORCECONTAINER_H */

