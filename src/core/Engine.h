/*
 * File:   Engine.h
 * Author: soad
 *
 * Created on June 2, 2011, 11:42 AM
 */

#ifndef ENGINE_H
#define	ENGINE_H

#include "Simulation.h"

class Simulation;

/**
 * @brief Piezas base de una simulación que define un proceso específico en un paso de la simulación.
 *
 * Los engines son las piezas intercambiables de cada simulacion que ejecutan una accion en un punto determinado del
 * ciclo de simulación. No confundir con los GeneralPurposeEngines que son para implementar acciones que necesitan ser
 * ejecutadas en varios puntos del ciclo de simulación.
 */
class Engine {
public:
	/**
	 * Ejecuta la acción asociada a este engine
	 */
	virtual void execute(Simulation* sim)=0;
	virtual ~Engine();
};

#endif	/* ENGINE_H */

