/*
 * File:   Circle.h
 * Author: soad
 *
 * Created on April 27, 2011, 10:15 AM
 */

#ifndef PARTICLE_H
#define	PARTICLE_H

#include "MathDefs.h"
#include <boost/shared_ptr.hpp>
#include "Shape.h"
#include "AABB.h"

/**
 * @brief Representa una prtícula en la simulación.
 *
 * Es una clase clave pues las partículas son los elemtos báscios de una simulacion.
 * Aquí se guarda la información del estado de la partícula y una referencia a la forma
 * geométrica de la misma.
 *
 * En un futuro se debe agregar una referencia a los parámetros de materiales.
 */
class Particle {

public:
	typedef int id_t;
	static const id_t ID_NONE;

	friend class ParticleContainer; ///Solo el contenedor de particulas puede cambiar el id.

	//Contructores
	Particle();
	Particle(VectorND cent);
	Particle(const Particle& orig);
	virtual ~Particle();


	VectorND getAcceleration() const {
		return acceleration;
	}

	void setAcceleration(VectorND acceleration) {
		this->acceleration = acceleration;
	}

	VectorND getAngularAcceleration() const {
		return angularAcceleration;
	}

	void setAngularAcceleration(VectorND angularAcceleration) {
		this->angularAcceleration = angularAcceleration;
	}

	VectorND getAngularVelocity() const {
		return angularVelocity;
	}

	void setAngularVelocity(VectorND angularVelocity) {
		this->angularVelocity = angularVelocity;
	}

	VectorND getCenter() const {
		return center;
	}

	void setCenter(VectorND center) {
		this->center = center;
	}

	VectorND getCenterBefore() const {
		return centerBefore;
	}

	void setCenterBefore(VectorND centerBefore) {
		this->centerBefore = centerBefore;
	}

	VectorND getFixedPoint() const {
		return fixedPoint;
	}

	void setFixedPoint(VectorND fixedPoint) {
		this->fixedPoint = fixedPoint;
	}

	id_t getId() const {
		return id;
	}

	MatrixNN getInertiaMoment() const {
		return inertiaMoment;
	}

	void setInertiaMoment(MatrixNN inertiaMoment) {
		this->inertiaMoment = inertiaMoment;
	}

	MatrixNN getInverseInertiaMoment() const {
		return inverseInertiaMoment;
	}

	void setInverseInertiaMoment(MatrixNN inverseInertiaMoment) {
		this->inverseInertiaMoment = inverseInertiaMoment;
	}

	Real getMass() const {
		return mass;
	}

	void setMass(Real mass) {
		this->mass = mass;
	}

	bool isMobile() const {
		return mobile;
	}

	void setMobile(bool mobile) {
		this->mobile = mobile;
	}

	VectorND getRotationalAngle() const {
		return rotationalAngle;
	}

	void setRotationalAngle(VectorND rotationalAngle) {
		this->rotationalAngle = rotationalAngle;
	}

	boost::shared_ptr<Shape> getShape() const {
		return shape;
	}

	void setShape(boost::shared_ptr<Shape> shape) {
		this->shape = shape;
	}

	VectorND getVelocity() const {
		return velocity;
	}

	void setVelocity(VectorND velocity) {
		this->velocity = velocity;
	}

	int getMaterialId() const {
		return materialId;
	}

	void setMaterialId(int materialId) {
		this->materialId = materialId;
	}

private:
	///Id de la partícula
	id_t id;
	///centro de la partícula
	VectorND center;
	//centro en el paso anterior
	VectorND centerBefore;

	///forma de la patícula
	boost::shared_ptr<Shape> shape;

	Real mass; ///masa
	bool mobile; ///indicador de movilidad de la partícula

	VectorND fixedPoint; ///punto fijo en la superficie de la partícula

	VectorND velocity; ///velocidad
	VectorND acceleration; ///aceleraciín
	VectorND rotationalAngle; ///íngulo de rotaciín
	VectorND angularVelocity;
	VectorND angularAcceleration;

	MatrixNN inertiaMoment; /// momento de inercia de la partícula
	MatrixNN inverseInertiaMoment; /// inversa del momento de inercia

	int materialId; ///material de la partícula

};

std::ostream& operator<<(std::ostream& os, const Particle& p);

#endif	/* PARTICLE_H */

