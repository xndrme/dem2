/*
 * File:   GeneralPurposeEngine.h
 * Author: soad
 *
 * Created on June 25, 2011, 11:31 AM
 */

#ifndef GENERALPURPOSEENGINE_H
#define	GENERALPURPOSEENGINE_H

#include "Simulation.h"

class Simulation;

/**
 * @brief Clase base para los Engines de proposito general, los cuales puede ejecutar codigo en varios puntos
 * del paso de simulación.
 */
class GeneralPurposeEngine {
public:
    /**
     * Este método se llama antes de iniciar la simulación para cada engine de propósito general. Es útil para los
     * engines que necesitan inicializar algunos datos antes la simulación.
     */
	virtual void init(Simulation* sim) {}

    /**
     * Este método se llama ANTES de ejecutar el paso actual de la simulación.
     */
	virtual void beforeStep(Simulation* sim) {}

    /**
     * Este método se llama justo antes de ejecutar el detector de collisiones.
     */
    virtual void beforeCollitions(Simulation* sim) {}

    /**
     * Este método se llama justo antes de ejecutar los modelos para las interacciones.
     */
    virtual void beforeInteractions(Simulation* sim) {}

    /**
     * Este método se llama justo antes de ejecutar el integrador.
     */
	virtual void beforeIntegration(Simulation* sim) {}

	virtual ~GeneralPurposeEngine();
private:
};

#endif	/* GENERALPURPOSEENGINE_H */

