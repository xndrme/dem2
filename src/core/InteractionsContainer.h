/*
 * File:   InteractionsContainer.h
 * Author: soad
 *
 * Created on July 4, 2011, 3:48 PM
 */

#ifndef INTERACTIONSCONTAINER_H
#define	INTERACTIONSCONTAINER_H

#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>
#include <list>
#include <boost/foreach.hpp>

#include "Interaction.h"

class Simulation;
/**
 * @brief Conteneder de interacciones. Permite insertar interaciones basadas en los índices de las partículas.
 *
 * Tiene en cuenta que (idx1, idx2) es lo mismo que (idx2, idx1), es decir que las interacciones son
 * reflexivas.
 */
class InteractionsContainer {
	typedef std::vector<boost::shared_ptr<Interaction> > ListT;

	///listado de interacciones
	ListT interactions;

	/**
	 * utilitario para acceder a las interacciones de una partícula específica
	 * es un vector indexado según id1 que guarda un mapeo entre id2 y la posición
	 * de la interacción id1<->id2 en la lista de intereacciones
	 */
	std::vector<std::map<Particle::id_t, size_t> > vecmap;

	size_t currentSize; ///sincronizado con interactions.size()

	boost::shared_ptr<Interaction> empty; ///intereacción vacía

public:

	///Marca de para la posible eliminación de una interacción
	typedef std::pair<Particle::id_t, Particle::id_t> PendingErase;

	///listado de interacciones para posible eliminación
	std::list<PendingErase> pendingsToErase;

	InteractionsContainer() : currentSize(0),  empty(boost::shared_ptr<Interaction>()) {}

	typedef ListT::iterator iterator;
	typedef ListT::const_iterator const_iterator;

	/**
	 * Limpia TODAS las interacciones sin ninguna verificación extra
	 */
	void clear();

	iterator begin() { return interactions.begin(); }
	iterator end() { return interactions.end();	}

	const_iterator begin() const { return interactions.begin(); }
	const_iterator end() const { return interactions.end();	}

	/**
	 * Crea una nueva interacción para las partículas con ids id1 y id2 y la
	 * inserta en el contenedor
	 */
	bool insert(Particle::id_t id1, Particle::id_t id2);

	/**
	 * Inserta la interacción i en el contenedor
	 */
	bool insert(const boost::shared_ptr<Interaction>& i);

	/**
	 * Elimina una interacción
	 * Los detectores de colisiones debería utilizar ErasePendings para eliminar las interaccviones
	 * marcadas
	 */
	bool erase(Particle::id_t id1, Particle::id_t id2);

	/**
	 * Busca una interacción dado el id de las partículas
	 */
	boost::shared_ptr<Interaction>& find(Particle::id_t id1, Particle::id_t id2);

	boost::shared_ptr<Interaction>& operator[](size_t id) {	return interactions[id]; }
	const boost::shared_ptr<Interaction>& operator[](size_t id) const {	return interactions[id]; }

	/**
	 * Cantidad de interacciones
	 */
	size_t size() const { return currentSize; }

	/**
	 * Elimina todas las interacciones que no son reales, en las cuales no hay interacción geométrica
	 * o física, es decir aquellas que no son importantes ni para el detector de colisiones ni para
	 * la ley constitutiva
	 */
	void eraseNonReal();

	/**
	 * Marca una interacción para su posible eliminación
	 */
	void requestErase(Particle::id_t id1, Particle::id_t id2);

	/**
	 * Recorre la lista de interacciones marcadas para eliminar y las elimina en caso de que así
	 * lo indique el predicado que se pasa como parámetro.
	 * OJO:
	 * Las clases que usen esto deben implementar un metodo ShoulBeErase(id1,id2,sim)
	 */
	template<class T> void erasePending(T* predicate, Simulation *sim) {

		BOOST_FOREACH(const PendingErase& p, pendingsToErase) {
			if (predicate->shouldBeErased(p.first, p.second, sim)) {
				erase(p.first, p.second);
			}
		}

		pendingsToErase.clear();
	}

private:

};

#endif	/* INTERACTIONSCONTAINER_H */

