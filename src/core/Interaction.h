/*
 * File:   Interaction.h
 * Author: soad
 *
 * Created on June 18, 2011, 11:32 AM
 */

#ifndef INTERACTION_H
#define	INTERACTION_H

#include "Particle.h"
#include "ContactGeometry.h"
/**
 * @brief Esta clase almacena información de una interacción específica.
 * Se usa para la implementacion de las distancias de Verlet.
 */
class Interaction {

	Particle::id_t id1;
	Particle::id_t id2;

	boost::shared_ptr<ContactGeometry> geom;

	///indicador para saber is la interaccion es cohesiva
	bool cohesive;

	VectorND predTangForce; //fuerza tagencial en el paso de tiempo anterior
	VectorND springPoint1; /// extremo1 del muelle para interacción cohesiva
	VectorND springPoint2; /// extremo1 del muelle para interacción cohesiva
	Real previousURN;
	Real previousURT;

public:
	Interaction(Particle::id_t id1, Particle::id_t id2, bool isCohesive = false);
	virtual ~Interaction();

	bool isCohesive() const	{
		return cohesive;
	}

	void setCohesive(bool cohesive)	{
		this->cohesive = cohesive;
	}

	VectorND getSpringPoint1() const {
		return springPoint1;
	}

	void setSpringPoint1(VectorND springPoint1)	{
		this->springPoint1 = springPoint1;
	}

	VectorND getSpringPoint2() const {
		return springPoint2;
	}

	void setSpringPoint2(VectorND springPoint2)	{
		this->springPoint2 = springPoint2;
	}

	VectorND getPredictedTangetialForce() const {
		return predTangForce;
	}

	void setPredictedTangetialForce(VectorND predTangForce) {
		this->predTangForce = predTangForce;
	}

	boost::shared_ptr<ContactGeometry>& getGeom() {
		return geom;
	}

	void setGeom(boost::shared_ptr<ContactGeometry> geom) {
		this->geom = geom;
	}

	Particle::id_t getId1() const {
		return id1;
	}

	Particle::id_t getId2() const {
		return id2;
	}

	Real getPreviousURN() const {
		return previousURN;
	}

	void setPreviousURN(Real previousURN) {
		this->previousURN = previousURN;
	}

	Real getPreviousURT() const {
		return previousURT;
	}

	void setPreviousURT(Real previousURT) {
		this->previousURT = previousURT;
	}

	bool isReal() {
		return (bool) geom;
	}

	void reset() {
		geom = boost::shared_ptr<ContactGeometry>();
	}


};

#endif	/* INTERACTION_H */

