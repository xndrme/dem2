/*
 * HouboltIntegrator.h
 *
 *  Created on: 2013-10-25
 *      Author: alvarojavier
 */

#ifndef HOUBOLTINTEGRATOR_H_
#define HOUBOLTINTEGRATOR_H_

#include "IntegratorEngine.h"

/**
 * @brief  Método de integración de Houbolt.
 *
 * Según la tesis de Irvin.
 */
class HouboltIntegrator: public IntegratorEngine {

	/**
	 * @brief Información extra sobre cada partícula necesaria en el método de Houbolt
	 */
	struct HouboltExtraData {
		VectorND posN2; /// Posición en dos pasos anteriores
		VectorND rotN1, rotN2; /// Rotación en pasos anteriores
		HouboltExtraData() :
				posN2(VectorND::Zero()), rotN1(VectorND::Zero()), rotN2(VectorND::Zero()) {
		}
	};

public:
	HouboltIntegrator();
	/**
	 * Inicializar la información extra sobre las partículas
	 * @param sim Simulación
	 */
	void init(Simulation *sim);
	void execute(Simulation *sim);
	virtual ~HouboltIntegrator();

private:
	std::vector<HouboltExtraData> extraData; ///contenedor de informacion extra para las partículas

};

#endif /* HOUBOLTINTEGRATOR_H_ */
