/*
 * File:   GearIntegrator.h
 * Author: soad
 *
 * Created on March 20, 2012, 11:11 AM
 */

#ifndef GEARINTEGRATOR_H
#define	GEARINTEGRATOR_H

#include "IntegratorEngine.h"

/**
 * @brief  Integrador según el método de integración de Gear
 *
 * La implementación sigue las especificaciones de la tesis de Irvin
 * capítulo 3 epígrafe 3.7.5
 */
class GearIntegrator: public IntegratorEngine {

	/**
	 * @brief Información extra por cada partícula que necesita este método
	 */
	struct GearExtraInfo {

		//posición
		VectorND predictedCenter; ///predicción del centro en la etapa predictiva del método de Gear
		VectorND predictedVelocity; ///predicción de la velocidad en la etapa predictiva del método de Gear
		VectorND predictedAcceleration;  ///predicción de la aceleración en la etapa predictiva del método de Gear
		VectorND rtd3; ///predicción de la tercera derivada del moviemiento en la etapa predictiva del método de Gear
		VectorND rtd4; ///predicción de la cuarta derivada del moviemiento en la etapa predictiva del método de Gear

		//rotación
		VectorND predictedRotationalAngle; ///predicción del águlo de rotación en la etapa predictiva del método de Gear
		VectorND predictedAngularVelocity; ///predicción de la velocidad angular en la etapa predictiva del método de Gear
		VectorND predictedAngularAcceleration; ///predicción de la aceleración angular en la etapa predictiva del método de Gear
		VectorND rtd3Rotational; ///predicción de la tercera derivada de la rotación en la etapa predictiva del método de Gear
		VectorND rtd4Rotational; ///predicción de la tercera derivada de la rotación en la etapa predictiva del método de Gear

		GearExtraInfo() {
			predictedCenter = predictedVelocity = predictedAcceleration = rtd3 = rtd4 = VectorND::Zero();
			predictedRotationalAngle = predictedAngularVelocity = predictedAngularAcceleration = rtd3Rotational = rtd4Rotational = VectorND::Zero();
		}

	};

public:
	GearIntegrator();
	/**
	 * Inicializar la información extra sobre las partículas
	 * @param sim Simulación
	 */
	virtual void init(Simulation* sim);
	/**
	 * Aquí se actualizar la inversa del tensor momento de inercia y se ejecuta el predictor
	 */
	virtual void beforeCollitions(Simulation* sim);
	//en un punto por aquí se calculan las fuerzas y velocidades teneindo en cuenta las interacciones
	/**
	 * Ejecución del corrector[correct(Particle& p, Real dt, VectorND force, VectorND torque)] en el método de Gear
	 * @param sim
	 */
	virtual void execute(Simulation* sim); //aquí se ejecutara el corrector
	virtual ~GearIntegrator();
	private:

	/**
	 * Predictor del método de Gear
	 *
	 * @param p Partícula
	 * @param dt Paso de tiempo
	 */
	void predict(Particle& p, Real dt);

	/**
	 * Corrector del método de Gear
	 *
	 * @param p Partícula
	 * @param dt Paso de tiempo
	 * @param force Fuerza resultante sobre la partícula
	 * @param torque Torque resultante sobre la partícula
	 */
	void correct(Particle& p, Real dt, VectorND force, VectorND torque);

	std::vector<GearExtraInfo> extraInfo; ///información extra sobre cada partícula necesaria para el método de Gear

};

#endif	/* GEARINTEGRATOR_H */

