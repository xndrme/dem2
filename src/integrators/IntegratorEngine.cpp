/*
 * IntegratorEngine.cpp
 *
 *  Created on: 2013-10-26
 *      Author: alvarojavier
 */

#include "IntegratorEngine.h"
#include <utils/MathUtils.h>

IntegratorEngine::IntegratorEngine() {
}

IntegratorEngine::~IntegratorEngine() {
}

void IntegratorEngine::updateInverseInertiaMoment(const boost::shared_ptr<Particle>& p, Simulation* sim) {
	MatrixNN rCN = MathUtils::getRotationMatrix(p,sim);

	//actualizar los tensores
	p->setInertiaMoment(rCN * p->getInertiaMoment() * rCN.transpose());
	p->setInverseInertiaMoment(p->getInertiaMoment().inverse());

	//actualizar punto fijo
	p->setFixedPoint(p->getCenter() + (p->getFixedPoint() - p->getCenterBefore())); //mover
	p->setFixedPoint(p->getCenter() +  rCN * (p->getFixedPoint() - p->getCenter()));//rotar en la nueva posición

}
