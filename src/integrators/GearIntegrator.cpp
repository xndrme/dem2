/*
 * File:   GearIntegrator.cpp
 * Author: soad
 *
 * Created on March 20, 2012, 11:11 AM
 */

#include "GearIntegrator.h"
#include <core/Particle.h>
#include <core/Simulation.h>
#include <core/MathDefs.h>

GearIntegrator::GearIntegrator() {
}

void GearIntegrator::init(Simulation* sim) {
	extraInfo.resize(sim->getParticles().size(), GearExtraInfo());
}


void GearIntegrator::beforeCollitions(Simulation* sim) {
	ParticleContainer& particles = sim->getParticles();
	Real timestep = sim->getTimeStep();
	const unsigned int size = particles.size();

	for (unsigned int i = 0; i < size; i++) {
		if (particles[i]->isMobile()) {
			predict(*particles[i], timestep);
		}
	}
}

void GearIntegrator::execute(Simulation* sim) {
	const unsigned int size = sim->getParticles().size();

	for (unsigned int i = 0; i < size; i++) {
		Particle* c1 = sim->getParticles()[i].get();

		if (c1->isMobile()) {
			correct(*c1, sim->getTimeStep(), sim->getForces().getForce(i), sim->getForces().getTorque(i));
			updateInverseInertiaMoment(sim->getParticles()[i],sim);
		}

	}
}

void GearIntegrator::predict(Particle& p, Real dt) {
	const Real a1 = dt; 			//     dt
	const Real a2 = a1 * dt / 2.0; 	// 1/2 dt^2
	const Real a3 = a2 * dt / 3.0; 	// 1/6 dt^3
	const Real a4 = a3 * dt / 4.0; 	// 1/24dt^4

	Particle::id_t id = p.getId();

	/* posición */
	extraInfo[id].predictedCenter =
			p.getCenter() +
			a1 * p.getVelocity() +
			a2 * p.getAcceleration() +
			a3 * extraInfo[id].rtd3 +
			a4 * extraInfo[id].rtd4;
	extraInfo[id].predictedVelocity =
			p.getVelocity() +
			a1 * p.getAcceleration() +
			a2 * extraInfo[id].rtd3 +
			a3 * extraInfo[id].rtd4;
	extraInfo[id].predictedAcceleration =
			p.getAcceleration() +
			a1 * extraInfo[id].rtd3 +
			a2 * extraInfo[id].rtd4;
	extraInfo[id].rtd3 =
			extraInfo[id].rtd3 +
			a1 * extraInfo[id].rtd4;

	/* rotación */
	extraInfo[id].predictedRotationalAngle =
			p.getRotationalAngle() +
			a1 * p.getAngularVelocity() +
			a2 * p.getAngularAcceleration() +
			a3 * extraInfo[id].rtd3Rotational +
			a4 * extraInfo[id].rtd4Rotational;
	extraInfo[id].predictedAngularVelocity =
			p.getAngularVelocity() +
			a1 * p.getAngularAcceleration() +
			a2 * extraInfo[id].rtd3Rotational +
			a3 * extraInfo[id].rtd4Rotational;
	extraInfo[id].predictedAngularAcceleration =
			p.getAngularAcceleration() +
			a1 * extraInfo[id].rtd3Rotational +
			a2 * extraInfo[id].rtd4Rotational;
	extraInfo[id].rtd3Rotational =
			extraInfo[id].rtd3Rotational +
			a1 * extraInfo[id].rtd4Rotational;

}

void GearIntegrator::correct(Particle& p, Real dt, VectorND force, VectorND torque) {
	static VectorND accel, corr;
	static VectorND accelRot, corrRot;
	const Real dtrez 	= 1.0/dt;

	//coeficientes con el factor tiempo
	const Real c0 		= (19.0/180.0) * (dt*dt);
	const Real c1 		= (3.0/8.0)    *  dt;
	//const Real c2 	= 1.0;
	const Real c3 		= (3.0/2.0)    *  dtrez;
	const Real c4 		=                (dtrez*dtrez);

	Particle::id_t id = p.getId();

	/* posición */
	accel = force / p.getMass(); //aceleración correcta!
	corr = accel - extraInfo[id].predictedAcceleration; //corrección

	p.setCenterBefore(p.getCenter());
	p.setCenter(extraInfo[id].predictedCenter + c0 * corr);
	p.setVelocity(extraInfo[id].predictedVelocity + c1 * corr);
	p.setAcceleration(accel);
	extraInfo[id].rtd3 = extraInfo[id].rtd3 + (c3 * corr);
	extraInfo[id].rtd4 = extraInfo[id].rtd4 + (c4 * corr);

	/* rotación */
	accelRot = p.getInverseInertiaMoment() * torque; //aceleración angular correcta
	corrRot = accelRot - extraInfo[id].predictedAngularAcceleration; //corrección

	p.setRotationalAngle(extraInfo[id].predictedRotationalAngle + c0 * corrRot);
	p.setAngularVelocity(extraInfo[id].predictedAngularVelocity + c1 * corrRot);
	p.setAngularAcceleration(accelRot);
	extraInfo[id].rtd3Rotational = extraInfo[id].rtd3Rotational + (c3 * corrRot);
	extraInfo[id].rtd4Rotational = extraInfo[id].rtd4Rotational + (c4 * corrRot);
}

GearIntegrator::~GearIntegrator() {
}

