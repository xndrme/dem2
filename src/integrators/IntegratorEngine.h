/*
 * IntegratorEngine.h
 *
 *  Created on: 2013-10-26
 *      Author: alvarojavier
 */

#ifndef INTEGRATORENGINE_H_
#define INTEGRATORENGINE_H_

#include <core/Engine.h>
#include <core/GeneralPurposeEngine.h>

/**
 * @brief  Esta clase modela un integrador en la simulación.
 *
 * Los integradores son un tipo especial de Engine que se encarga del proceso de integración,
 * por los requerimientos especiales de los métodos de integración esta clase también hereda
 * de GeneralPurposeEngine para que los integradore tengan más opciones de ejecución de código.
 */
class IntegratorEngine : public Engine, public GeneralPurposeEngine {
public:
	IntegratorEngine();
	virtual ~IntegratorEngine();
protected:
	/**
	 * Este método utilitario es para actualizar la inversa de los tensores momento de inercia
	 * y el punto fioj sobre la partícula
	 */
	void updateInverseInertiaMoment(const boost::shared_ptr<Particle>& p, Simulation* sim);
};

#endif /* INTEGRATORENGINE_H_ */
