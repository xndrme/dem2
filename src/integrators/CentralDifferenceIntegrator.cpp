/*
 * CentralDifferenceIntegrator.cpp
 *
 *  Created on: ene 17, 2015
 *      Author: xndrme
 */

#include "CentralDifferenceIntegrator.h"

CentralDifferenceIntegrator::CentralDifferenceIntegrator() {
}

CentralDifferenceIntegrator::~CentralDifferenceIntegrator() {
}

void CentralDifferenceIntegrator::execute(Simulation* sim) {
	VectorND center,vel,accel;
	VectorND rotAng,angVel,angAccel;
	Particle *p;
	double dt = sim->getTimeStep();
	for(unsigned int i=0;i<sim->getParticles().size();i++) {
		p = sim->getParticles()[i].get();

		//verificar si es una partícula móvil o no
		if(!p->isMobile())
			continue;

		//calcular la aceleración en este instante
		accel = sim->getForces().getForce(p->getId()) / p->getMass();
		vel = p->getVelocity() + .5*dt*(accel + p->getAcceleration());
		center = p->getCenter() + dt*p->getVelocity() + .5*dt*dt*p->getAcceleration();

		p->setCenterBefore(p->getCenter());
		p->setCenter(center);
		p->setVelocity(vel);
		p->setAcceleration(accel);

		//obtener la acelaración angular
		angAccel = p->getInverseInertiaMoment() * sim->getForces().getTorque(p->getId());
		//TODO: OJO reguntar a Irvin!!!!! verificar con la formulación en su tesis!!!
		//el problema es que en la tesis toman la velocidad angular del paso anterior
		//es decir el paso n-1 asi:  ωn-1+2Δtώn para eso habría que guardar las velocidades
		//de un paso anterior aqui en este método!
		angVel = p->getAngularVelocity() + 2*dt*p->getAngularAcceleration();

		p->setAngularVelocity(angVel);
		p->setAngularAcceleration(angAccel);
		p->setRotationalAngle(angVel*dt);

		updateInverseInertiaMoment(sim->getParticles()[i],sim);
	}
}

