/*
 * HouboltIntegrator.cpp
 *
 *  Created on: 2013-10-25
 *      Author: alvarojavier
 */

#include "HouboltIntegrator.h"

HouboltIntegrator::HouboltIntegrator() {
}

void HouboltIntegrator::execute(Simulation* sim) {
	VectorND center,vel,accel;
	VectorND rotAng,angVel,angAccel;
	Particle *p;
	double dt = sim->getTimeStep();
	for(unsigned int i=0;i<sim->getParticles().size();i++) {
		p = sim->getParticles()[i].get();

		//verificar si es una partícula móvil o no
		if(!p->isMobile())
			continue;

		//calcular la aceleración en este instante
		accel = sim->getForces().getForce(p->getId()) / p->getMass();
		//calcular el nuevo centro
		center = 0.5 * (accel*dt*dt + 5.0*p->getCenter() - 4.0*p->getCenterBefore() + extraData[p->getId()].posN2);
		//calcular la nueva velocidad
		vel = (11.0*center - 18.0*p->getCenter() + 9.0*p->getCenterBefore() - 2.0*extraData[p->getId()].posN2)/(6.0*dt);

		//actualizar las posiciones
		extraData[p->getId()].posN2 = p->getCenterBefore();
		p->setCenterBefore(p->getCenter());
		p->setCenter(center);
		p->setVelocity(vel);
		p->setAcceleration(accel);

		//obtener la acelaración angular
		angAccel = p->getInverseInertiaMoment() * sim->getForces().getTorque(p->getId());
		angVel = (6.0*dt*angAccel + 18.0*p->getAngularVelocity() - 9*extraData[p->getId()].rotN1 + 2*extraData[p->getId()].rotN2)/11.0;

		//actualización de movimientos angulares
		extraData[p->getId()].rotN2 = extraData[p->getId()].rotN1;
		extraData[p->getId()].rotN1 = p->getAngularVelocity();
		p->setAngularVelocity(angVel);
		p->setAngularAcceleration(angAccel);
		p->setRotationalAngle(angVel*dt);

		updateInverseInertiaMoment(sim->getParticles()[i],sim);

	}

}

void HouboltIntegrator::init(Simulation* sim) {
	const ParticleContainer& particles = sim->getParticles();
	extraData.resize(particles.size());
	for(unsigned int i=0;i<extraData.size();i++) {
		particles[i]->setCenterBefore(particles[i]->getCenter());
		extraData[i].posN2 = particles[i]->getCenter();
		extraData[i].rotN1 = particles[i]->getAngularVelocity();
		extraData[i].rotN2 = particles[i]->getAngularVelocity();
	}
}

HouboltIntegrator::~HouboltIntegrator() {
}

