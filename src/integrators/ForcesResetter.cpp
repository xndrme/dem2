/* 
 * File:   ForcesResetter.cpp
 * Author: soad
 * 
 * Created on June 2, 2011, 12:28 PM
 */

#include "ForcesResetter.h"

void ForcesResetter::execute(Simulation* sim) {
	sim->getForces().reset();
}

ForcesResetter::~ForcesResetter() {
}

