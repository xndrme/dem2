/*
 * CentralDifferenceIntegrator.h
 *
 *  Created on: ene 17, 2015
 *      Author: xndrme
 */

#ifndef CENTRALDIFFERENCEINTEGRATOR_H_
#define CENTRALDIFFERENCEINTEGRATOR_H_

#include "IntegratorEngine.h"

/**
 * @brief Integrador de Diferencias Finitas Central, tesis de Irvin
 */
class CentralDifferenceIntegrator: public IntegratorEngine {
public:
	CentralDifferenceIntegrator();
	void execute(Simulation *sim);
	virtual ~CentralDifferenceIntegrator();
};

#endif /* CENTRALDIFFERENCEINTEGRATOR_H_ */
