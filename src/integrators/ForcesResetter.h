/*
 * File:   ForcesResetter.h
 * Author: soad
 *
 * Created on June 2, 2011, 12:28 PM
 */

#ifndef FORCESRESETTER_H
#define	FORCESRESETTER_H

#include <core/Engine.h>

/**
 * @brief Reseteador de fuerzas.
 *
 * Ahora mismo no es muy inteligente pero en un futuro podría especializarse en dependencia de la complejidad de los
 * modelos y de cuantos pasos necesiten para calcular las fuerzas.
 */
class ForcesResetter: public Engine {
public:
	virtual void execute(Simulation* sim);
	virtual ~ForcesResetter();
};

#endif	/* FORCESRESETTER_H */

