/*
 * IrvinModelWithDamping.h
 *
 *  Created on: 08/05/2015
 *      Author: xndrme
 */

#ifndef MODELS_IRVINMODELWITHDAMPING_H_
#define MODELS_IRVINMODELWITHDAMPING_H_

#include <core/ModelEngine.h>
#include <core/Dispatcher.h>
#include <core/Simulation.h>
#include <shapes/Sphere.h>
#include <shapes/Plane.h>
#include <cstdio>
#include <utils/MathUtils.h>

using namespace std;
/**
 * @brief Esta clase implementa el modelo de la tesis de Irvin con Damping.
 */
class IrvinModelWithDamping: public ModelEngine {
public:
	IrvinModelWithDamping();
	virtual ~IrvinModelWithDamping();

	virtual void execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel);

};

#endif /* MODELS_IRVINMODELWITHDAMPING_H_ */
