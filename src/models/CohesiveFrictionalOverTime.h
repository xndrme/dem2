/*
 * CohesiveFrictionalOverTime.h
 *
 *  Created on: 05/05/2015
 *      Author: xndrme
 */

#ifndef MODELS_COHESIVEFRICTIONALOVERTIME_H_
#define MODELS_COHESIVEFRICTIONALOVERTIME_H_

#include <core/ModelEngine.h>
#include <core/Dispatcher.h>
#include <core/Simulation.h>
#include <shapes/Sphere.h>
#include <shapes/Plane.h>
#include <cstdio>
#include <utils/MathUtils.h>

using namespace std;

/**
 * @brief Modelo cohesivo friccional con variación en el tiempo
 */
class CohesiveFrictionalOverTime: public ModelEngine {

	/**
	 * @brief Material especial necesario para las constantes phi y c en este modelo
	 */
	class PrivateMaterial : public Material {
	public:
		Real phi;
		Real c;
		/**
		 * Implementación de Material::setProperty(string,double) para guardar phi y c en la lectura.
		 *
		 * @param name nombre de la propiedad (solo le interesa "phi" y "c")
		 * @param value valor para la propiedad
		 *
		 */
		void setProperty(std::string name, Real value);
		PrivateMaterial() : phi(0),c(0) {}
		~PrivateMaterial() {}
	};

public:
	CohesiveFrictionalOverTime();
	virtual ~CohesiveFrictionalOverTime();

	Material* getMaterialInstance();
	virtual void execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel);

};

#endif /* MODELS_COHESIVEFRICTIONALOVERTIME_H_ */
