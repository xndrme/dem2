/*
 * File:   InteractionsHandler.h
 * Author: soad
 *
 * Created on June 25, 2011, 10:28 AM
 */

#ifndef DEFAULTINTERACTIONSMODEL_H
#define	DEFAULTINTERACTIONSMODEL_H

#include <core/ModelEngine.h>
#include <core/Dispatcher.h>
#include <core/Simulation.h>
#include <shapes/Sphere.h>
#include <shapes/Plane.h>
#include <cstdio>
#include <utils/MathUtils.h>

using namespace std;


/**
 * @brief  Esta clase implementa el modelo de la tesis de Irvin para manejar las interacciones entre las partículas
 *
 */
class DefaultInteractionsModel: public ModelEngine {
public:
	DefaultInteractionsModel() {}
	virtual ~DefaultInteractionsModel();

	virtual void execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel);

};

#endif	/* DEFAULTINTERACTIONSMODEL_H */

