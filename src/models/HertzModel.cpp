//
// Created by xndrme on 15/05/2015.
//

#include "HertzModel.h"

using namespace boost;

void HertzModel::execute(boost::shared_ptr<Interaction> inter, Simulation *sim, bool isInterface, Interface interface, int partIdModel) {

    const ParticleContainer& particles = sim->getParticles();

    shared_ptr<Particle> p1 = particles[inter->getId1()];
    shared_ptr<Particle> p2 = particles[inter->getId2()];

    /******PARTE GEOM�TRICA DE LA INTERFAZ********/
    bool swap = false; //para saber si hay que intercambiar el orden de las partículas
    bool existHandler = false; //para saber si existe un manipulador de este tipo de interacción
    //obtener la geometr�a de contacto
    shared_ptr<GeometryFunctor> geomFunctor = geomDispacher.dispatch(p1->getShape().get(), p2->getShape().get(), swap, existHandler);

    if (!existHandler) {
        sim->getInteractions().requestErase(p1->getId(), p2->getId());
        return;
    }

    //manejar el swap en el caso de que sea asim�trica la interacción (Plano-Esfera)
    //si las dos partículas son iguales no hay problema
    if(swap) {
        p1 = particles[inter->getId2()];
        p2 = particles[inter->getId1()];
    }

    //ahora inicializar la geometr�a de interacción
    if (!geomFunctor->go(p1, p2, inter->getGeom())) {
        //si no se pudo entonces marcar como pendiente para borrar
        sim->getInteractions().requestErase(p1->getId(), p2->getId());
        return;
    }

    //geometr�a de contacto
    boost::shared_ptr<ContactGeometry> geom = inter->getGeom();

    /******PARTE F�SICA DE LA interacción********/

    Real kn,kt,knc;
    boost::shared_ptr<Material> m1 = sim->getMaterials()[p1->getMaterialId()];
    boost::shared_ptr<Material> m2 = sim->getMaterials()[p2->getMaterialId()];

    knc = (p1->getId()==partIdModel)? m1->knc : m2->knc;
    kt = (p1->getId()==partIdModel)? m1->kt : m2->kt;

    //vectores de fuerza
    VectorND contactForce = VectorND::Zero();
    VectorND normalForce, tangencialForce;
    VectorND vRT; //Velocidad relativa tangencial.

    if(inter->isCohesive()) {

    } else {

        Real uRN = geom->getURN(); //penetraci�n

        Real r1,r2;
        AABB box = p1->getShape()->getAABB(p1->getCenter());
        r1= (box.getMax()-box.getMin()).maxCoeff()/2.0;
        box = p2->getShape()->getAABB(p2->getCenter());
        r2 = (box.getMax()-box.getMin()).maxCoeff()/2.0;

        //radio efectivo
        Real effR = 2.0*r1*r2/(r1+r2);

        //la interacción hay que analizarla si se est�n acercando las partículas; si no, es que ya interactuaron y
        //se est�n alejando, de todas formas la interacción no se borra hasta que la penetraci�n haya desaparecido.
        if ((uRN >= 0) && (geom->isApproaching())) {
            //fprintf(f,"Interaccion Acercandose: %d %d\n",p1->getId(), p2->getId());
            //fflush(f);

            VectorND normal = geom->getNormal();
            VectorND contactPoint = geom->getContactPoint();

            kn = knc*std::sqrt(2.0*effR*uRN);
            //la fuerza normal es de repulsi�n y es igual al vector normal con longitud uRN multiplicado por
            //la constante de rigidez normal a compresi�n
            normalForce = kn * (-uRN) * normal;

            VectorND angularVelocity2 = p2->getAngularVelocity();
            VectorND radioVector2 = contactPoint - p2->getCenter();
            VectorND tangencialVelocity2 = angularVelocity2.cross(radioVector2);

            VectorND angularVelocity1 = p1->getAngularVelocity();
            VectorND radioVector1 = contactPoint - p1->getCenter();
            VectorND tangencialVelocity1 = angularVelocity1.cross(radioVector1);

            kt = kt*std::pow(effR,1.0/3.0) * std::pow(normalForce.norm(),1.0/3.0);
            tangencialForce = kt * normal;

            contactForce = normalForce + tangencialForce;

            VectorND momentoContacto  = radioVector1.cross(tangencialForce);
            VectorND momentoContacto2 = radioVector2.cross(tangencialForce);

            sim->getForces().addForce(p1->getId(), contactForce);
            sim->getForces().addTorque(p1->getId(), momentoContacto);

            sim->getForces().addForce(p2->getId(), -contactForce);
            sim->getForces().addTorque(p2->getId(), -momentoContacto2);

        } else {
            //si la penetraci�n es negativa es porque en realidad lo que hay es separaci�n
            //as� que no hay nada que hacer en el contacto
            if (uRN <= 0) {
                //marcar para borrar esta interacción
                sim->getInteractions().requestErase(p1->getId(), p2->getId());
            }
        }
    }

}
