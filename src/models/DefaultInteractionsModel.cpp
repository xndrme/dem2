/* 
 * File:   InteractionsHandler.cpp
 * Author: soad
 * 
 * Created on June 25, 2011, 10:28 AM
 */

#include "DefaultInteractionsModel.h"
#include <core/Interaction.h>
#include <shapes/Sphere.h>
#include <cstdio>
#include <boost/shared_ptr.hpp>
#include <iostream>

using namespace std;
using namespace boost;

void DefaultInteractionsModel::execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel){

	const ParticleContainer& particles = sim->getParticles();

	shared_ptr<Particle> p1 = particles[inter->getId1()];
	shared_ptr<Particle> p2 = particles[inter->getId2()];

	/******PARTE GEOMÉTRICA DE LA INTERFAZ********/
	bool swap = false; //para saber si hay que intercambiar el orden de las partículas
	bool existHandler = false; //para saber si existe un manipulador de este tipo de interacción
	//obtener la geometría de contacto
	shared_ptr<GeometryFunctor> geomFunctor = geomDispacher.dispatch(p1->getShape().get(), p2->getShape().get(), swap, existHandler);

	if (!existHandler) {
		sim->getInteractions().requestErase(p1->getId(), p2->getId());
		return;
	}

	//manejar el swap en el caso de que sea asimétrica la interacción (Plano-Esfera)
	//si las dos partículas son iguales no hay problema
	if(swap) {
		p1 = particles[inter->getId2()];
		p2 = particles[inter->getId1()];
	}

	//ahora inicializar la geometría de interacción
	if (!geomFunctor->go(p1, p2, inter->getGeom())) {
		//si no se pudo entonces marcar como pendiente para borrar
		sim->getInteractions().requestErase(p1->getId(), p2->getId());
		return;
	}

	//geometría de contacto
	boost::shared_ptr<ContactGeometry> geom = inter->getGeom();

	/******PARTE FÍSICA DE LA INTERACCIÓN********/

	Real knc,knt,kt,rn,rt,coulomb;
	boost::shared_ptr<Material> m1 = sim->getMaterials()[p1->getMaterialId()];
	boost::shared_ptr<Material> m2 = sim->getMaterials()[p2->getMaterialId()];
	//si es una interfaz
	if(isInterface) {
		//realizar ponderaciones de los materiales
		knc = 2.0*m1->knc*m2->knc/(m1->knc+m2->knc);
		knt = 2.0*m1->knt*m2->knt/(m1->knt+m2->knt);
		kt = 2.0*m1->kt*m2->kt/(m1->kt+m2->kt);
		coulomb = 2.0*m1->coulomb*m2->coulomb/(m1->coulomb+m2->coulomb);
		//obtener los valores específicos de rn y rt para esta interfaz
		rn = interface.rn;
		rt = interface.rt;
	} else {//si no obetener los materiales
		knc = m1->knc;
		knt = m1->knt;
		kt = m1->kt;
		rn = m1->rn;
		rt = m1->rt;
		coulomb = m1->coulomb;
	}

	//vectores de fuerza
	VectorND contactForce = VectorND::Zero();
	VectorND normalForce, tangencialForce;
	VectorND vRT; //Velocidad relativa tangencial.

	if(inter->isCohesive()) {

		//Actualización de los extremos del muelle
		//primero actualizar la posición
		inter->setSpringPoint1(inter->getSpringPoint1() + (p1->getVelocity() * sim->getTimeStep()));
		inter->setSpringPoint2(inter->getSpringPoint2() + (p2->getVelocity() * sim->getTimeStep()));
		//luego la rotación
		MatrixNN rCN1 = MathUtils::getRotationMatrix(p1,sim);
		inter->setSpringPoint1(p1->getCenter() + rCN1 * (inter->getSpringPoint1() - p1->getCenter()));
		MatrixNN rCN2 = MathUtils::getRotationMatrix(p2,sim);
		inter->setSpringPoint2(p2->getCenter() + rCN2 * (inter->getSpringPoint2() - p2->getCenter()));

		Real uRN = geom->getURN(); //penetración

		//si la penetración es negativa, o sea que hay separación,
		//y además esta separación es mayor que la distancia mínima para cohesión
		if((-uRN) > sim->getCohesiveDistance()) {
			//eliminar esta interacción cohesiva!
			inter->setCohesive(false); //criterio 1 ep 3.9.3.1 tesis de Irvin
		} else {
			//si llegamos aquí es porque la penetración/separación está dentro de los límites

			VectorND spring = inter->getSpringPoint2() - inter->getSpringPoint1();
			VectorND contactPoint = geom->getContactPoint();

			VectorND normal = geom->getNormal();

			VectorND tangent = (spring - (spring.dot(normal) * normal));
			if (tangent.norm() > MathDefs::EPSILON) {
				tangent = tangent.normalized();
			} else {
				tangent = VectorND::Zero();
			}

			//si hay compresión usar la rigidez a compresión
			//si hay tracción usar la rigidez a la tracción
			Real kn = geom->isApproaching() ? knc : knt;

			normalForce = kn * (-uRN) * normal;

			//si hay tracción: se alejan, y se rompe la rigidez a tracción
			if((uRN < inter->getPreviousURN()) && (normalForce.norm() > rn) ) {
				//entonces deja de ser un contacto cohesivo
				inter->setCohesive(false); //criterio 2 ep 3.9.3.1 tesis de Irvin
			}

			VectorND angularVelocity2 = p2->getAngularVelocity();
			VectorND radioVector2 = contactPoint - p2->getCenter();
			VectorND tangencialVelocity2 = angularVelocity2.cross(radioVector2);

			VectorND angularVelocity1 = p1->getAngularVelocity();
			VectorND radioVector1 = contactPoint - p1->getCenter();
			VectorND tangencialVelocity1 = angularVelocity1.cross(radioVector1);

			vRT = (p2->getVelocity() + tangencialVelocity2	- (p1->getVelocity() + tangencialVelocity1));
			vRT = vRT - (vRT.dot(normal) * normal);

			VectorND predictedTangencialForce = inter->getPredictedTangetialForce() - kt * vRT * sim->getTimeStep();

			Real miuF = coulomb * normalForce.norm();
			if(predictedTangencialForce.norm() < (miuF + MathDefs::EPSILON /*para cuando es cero Fnormal*/)) {
				//se mantiene el contacto cohesivo
				tangencialForce = predictedTangencialForce;
			} else {
				//se rompe el contacto cohesivo
				//TODO: Ojo preguntar a Irvin este detalle
				if(predictedTangencialForce.norm() > MathDefs::EPSILON) {
					tangencialForce = miuF * predictedTangencialForce.normalized();
				} else {
					tangencialForce = VectorND::Zero();
				}
				inter->setCohesive(false); //criterio 3 ep 3.9.3.1 tesis de Irvin
			}

			//si la fuerza tangencial sobrepasa el umbral de rigidez tangencial
			if(tangencialForce.norm() > rt) {
				//entonces se rompe el contacto cohesivo
				inter->setCohesive(false); //criterio 3 ep 3.9.3.1 tesis de Irvin
			}

			//ahora actualizar los datos de la interacción necesarios para el próximo paso
			inter->setPredictedTangetialForce(predictedTangencialForce);
			inter->setPreviousURN(uRN);
			//inter->setPreviousURT(uRT);

			contactForce = normalForce + tangencialForce;

			VectorND momentoContacto  = radioVector1.cross(tangencialForce);
			VectorND momentoContacto2 = radioVector2.cross(tangencialForce);

			sim->getForces().addForce(p1->getId(), contactForce);
			sim->getForces().addTorque(p1->getId(), momentoContacto);

			sim->getForces().addForce(p2->getId(), -contactForce);
			sim->getForces().addTorque(p2->getId(), -momentoContacto2);

		}

	} else {

		Real uRN = geom->getURN(); //penetración

		//la interacción hay que analizarla si se están acercando las partículas; si no, es que ya interactuaron y
		//se están alejando, de todas formas la interacción no se borra hasta que la penetración haya desaparecido.
		if ((uRN >= 0) && (geom->isApproaching())) {
			//fprintf(f,"Interaccion Acercandose: %d %d\n",p1->getId(), p2->getId());
			//fflush(f);

			VectorND normal = geom->getNormal();
			VectorND contactPoint = geom->getContactPoint();

			//la fuerza normal es de repulsión y es igual al vector normal con longitud uRN multiplicado por
			//la constante de rigidez normal a compresión
			normalForce = knc * (-uRN) * normal;

			VectorND angularVelocity2 = p2->getAngularVelocity();
			VectorND radioVector2 = contactPoint - p2->getCenter();
			VectorND tangencialVelocity2 = angularVelocity2.cross(radioVector2);

			VectorND angularVelocity1 = p1->getAngularVelocity();
			VectorND radioVector1 = contactPoint - p1->getCenter();
			VectorND tangencialVelocity1 = angularVelocity1.cross(radioVector1);

			vRT = (p2->getVelocity() + tangencialVelocity2	- (p1->getVelocity() + tangencialVelocity1));
			vRT = vRT - (vRT.dot(normal) * normal);

			//como este es el caso de no cohesivo es decir con deslizamiento
			if (vRT.norm() > MathDefs::EPSILON)
				//aplicamos esta fórmula
				tangencialForce = coulomb * normalForce.norm() * vRT.normalized();
			else
				tangencialForce = VectorND::Zero();

			contactForce = normalForce + tangencialForce;

			VectorND momentoContacto  = radioVector1.cross(tangencialForce);
			VectorND momentoContacto2 = radioVector2.cross(tangencialForce);

			sim->getForces().addForce(p1->getId(), contactForce);
			sim->getForces().addTorque(p1->getId(), momentoContacto);

			sim->getForces().addForce(p2->getId(), -contactForce);
			sim->getForces().addTorque(p2->getId(), -momentoContacto2);

		} else {
			//si la penetración es negativa es porque en realidad lo que hay es separación
			//así que no hay nada que hacer en el contacto
			if (uRN <= 0) {
				//marcar para borrar esta interacción
				sim->getInteractions().requestErase(p1->getId(), p2->getId());
			}
		}
	}
}

DefaultInteractionsModel::~DefaultInteractionsModel() {
}

