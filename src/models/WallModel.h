/*
 * WallModel.h
 *
 *  Created on: 01/05/2015
 *      Author: xndrme
 */

#ifndef MODELS_WALLMODEL_H_
#define MODELS_WALLMODEL_H_

#include <core/ModelEngine.h>

/**
 * @brief Modelo básico para pared
 */
class WallModel : public ModelEngine {
public:
	WallModel();
	virtual ~WallModel();

	virtual void execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel);

};

#endif /* MODELS_WALLMODEL_H_ */
