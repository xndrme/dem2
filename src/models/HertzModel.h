//
// Created by xndrme on 15/05/2015.
//

#ifndef DEM2_HERTZMODEL_H
#define DEM2_HERTZMODEL_H


#include <core/ModelEngine.h>

/**
 * @brief Esta clase implementa el modelo de Hertz-Mindlin
 *
 * IMPORTANTE:
 *  * el knc del material debe ser \f$\frac{2G}{3(1-v)}\f$
 *  * el knt del material debe ser \f$\frac{2G^{\frac{2}{3}}\sqrt[3]{3(1-v)}}{2-v}\f$
 * donde \f$G\f$ es el módulo de young del material y \f$v\f$ es el radio de Poisson
 */;
class HertzModel : public ModelEngine{

public:
    virtual void execute(boost::shared_ptr<Interaction> inter, Simulation *sim, bool isInterface, Interface interface,
                         int partIdModel);

    virtual ~HertzModel() { }
};


#endif //DEM2_HERTZMODEL_H

