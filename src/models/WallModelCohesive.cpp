/*
 * WallModelCohesive.cpp
 *
 *  Created on: 05/05/2015
 *      Author: xndrme
 */

#include "WallModelCohesive.h"

WallModelCohesive::WallModelCohesive() {}

WallModelCohesive::~WallModelCohesive() {
}

void WallModelCohesive::execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel){

	const ParticleContainer& particles = sim->getParticles();

	shared_ptr<Particle> p1 = particles[inter->getId1()];
	shared_ptr<Particle> p2 = particles[inter->getId2()];

	/******PARTE GEOMÉTRICA DE LA INTERFAZ********/
	bool swap = false; //para saber si hay que intercambiar el orden de las partículas
	bool existHandler = false; //para saber si existe un manipulador de este tipo de interacción
	//obtener la geometría de contacto
	shared_ptr<GeometryFunctor> geomFunctor = geomDispacher.dispatch(p1->getShape().get(), p2->getShape().get(), swap, existHandler);

	if (!existHandler) {
		sim->getInteractions().requestErase(p1->getId(), p2->getId());
		return;
	}

	//manejar el swap en el caso de que sea asimétrica la interacción (Plano-Esfera)
	//si las dos partículas son iguales no hay problema
	if(swap) {
		p1 = particles[inter->getId2()];
		p2 = particles[inter->getId1()];
	}

	//ahora inicializar la geometría de interacción
	if (!geomFunctor->go(p1, p2, inter->getGeom())) {
		//si no se pudo entonces marcar como pendiente para borrar
		sim->getInteractions().requestErase(p1->getId(), p2->getId());
		return;
	}

	//geometría de contacto
	boost::shared_ptr<ContactGeometry> geom = inter->getGeom();

	/******PARTE FÍSICA DE LA INTERACCIÓN********/

	Real knc,coulomb;
	boost::shared_ptr<Material> m = sim->getMaterials()[p1->getId() == partIdModel? p1->getMaterialId(): p2->getMaterialId()];
	//si es una interfaz
	if(isInterface) {
		knc = m->knc;
		coulomb = m->coulomb;
	} else {
		//si no es interfaz no se hace nada porque la pared solo puede interactuar en modo interfaz
		return;
	}

	//vectores de fuerza
	VectorND contactForce = VectorND::Zero();
	VectorND normalForce, tangencialForce;
	VectorND vRT; //Velocidad relativa tangencial.


	Real uRN = geom->getURN(); //penetración

	if ( ((uRN >= 0) && geom->isApproaching()) || //si la penetración es positiva y se esta acercando la partícula a la pared, debemos calcular la interacción
			//de lo contrario no, porque tenemos que ya interacturaron y la partícula se está alejando de la pared
		((uRN<0) && (-uRN<sim->getCohesiveDistance())) //si la penetración es negativa es que la partícula se separa de la pared y en este caso
		//hay que ver si la distancia es menor de lo que podemos considerar una distancia cohesiva.
		) {

		VectorND normal = geom->getNormal();
		VectorND contactPoint = geom->getContactPoint();

		//la fuerza normal es de repulsión y es igual al vector normal con longitud uRN multiplicado por
		//la constante de rigidez normal a compresión
		normalForce = knc * (-uRN) * normal;

		VectorND angularVelocity2 = p2->getAngularVelocity();
		VectorND radioVector2 = contactPoint - p2->getCenter();
		VectorND tangencialVelocity2 = angularVelocity2.cross(radioVector2);

		VectorND angularVelocity1 = p1->getAngularVelocity();
		VectorND radioVector1 = contactPoint - p1->getCenter();
		VectorND tangencialVelocity1 = angularVelocity1.cross(radioVector1);

		vRT = (p2->getVelocity() + tangencialVelocity2	- (p1->getVelocity() + tangencialVelocity1));
		vRT = vRT - (vRT.dot(normal) * normal);

		//como este es el caso de no cohesivo es decir con deslizamiento
		if (vRT.norm() > MathDefs::EPSILON)
			//aplicamos esta fórmula
			tangencialForce = coulomb * normalForce.norm() * vRT.normalized();
		else
			tangencialForce = VectorND::Zero();

		contactForce = normalForce + tangencialForce;

		VectorND momentoContacto  = radioVector1.cross(tangencialForce);
		VectorND momentoContacto2 = radioVector2.cross(tangencialForce);

		sim->getForces().addForce(p1->getId(), contactForce);
		sim->getForces().addTorque(p1->getId(), momentoContacto);

		sim->getForces().addForce(p2->getId(), -contactForce);
		sim->getForces().addTorque(p2->getId(), -momentoContacto2);

	} else {
		//en el único caso que no se borra la interacción es cuando la uRN es positiva pero
		//se estan alejando las partículas, o sea que la posible interacción no ha terminado
		if ( (uRN<0) && (-uRN<sim->getCohesiveDistance()) ) {
			//marcar para borrar esta interacción
			sim->getInteractions().requestErase(p1->getId(), p2->getId());
		}
	}

}


