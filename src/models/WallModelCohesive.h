/*
 * WallModelCohesive.h
 *
 *  Created on: 05/05/2015
 *      Author: xndrme
 */

#ifndef MODELS_WALLMODELCOHESIVE_H_
#define MODELS_WALLMODELCOHESIVE_H_

#include "boost/shared_ptr.hpp"
#include <core/Simulation.h>
#include <core/Interaction.h>
#include <core/Interface.h>

using namespace boost;

/**
 * @brief Modelo para pared con interacciones cohesivas
 */
class WallModelCohesive : public ModelEngine {
public:
	WallModelCohesive();
	virtual ~WallModelCohesive();

	virtual void execute(boost::shared_ptr<Interaction> inter, Simulation* sim, bool isInterface, Interface interface, int partIdModel);
};

#endif /* MODELS_WALLMODELCOHESIVE_H_ */
