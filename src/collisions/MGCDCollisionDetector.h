/*
 * MGCDCollisionDetector.h
 *
 *  Created on: abr 5, 2014
 *      Author: Alvaro Javier
 */

#ifndef MGCDCOLLISIONDETECTOR_H_
#define MGCDCOLLISIONDETECTOR_H_

#include <core/Engine.h>
#include "mgcd.h"
#include <cstdio>

using namespace std;

/**
 * @brief Esta clase implementa la busqueda de posibles colisiones utilizando la biblioteca "Multi Grid Contact Detection".
 *
 * Esta biblioteca tiene dos algoritmos para la búsqueda de colisiones: NBS y Screening.
 * Se puede escoger cual algoritmo utilizar en cada grilla de la busqueda de colisiones.
 * La forma más sencilla de utilizar es con una sola grilla cuyas celdas abarquen coimpletamente a la partícula de mayor
 * tamaño, pero este modo no es bueno cuando las partículas tienen una variación de tamaño muy grande.
 *
 * La forma de entrar la configuración de este método en el CsvSimulationLoader es la siguiente:
 *  # coll: mgcd,cell-size[, cell-size]*,[n|s|a]+
 *  donde cell-size es el tamaño de la celda, se puede definir una lista de tamaños que hacen que el algoritmo se
 *  ejecute con multiples pasos, uno para cada tamaño de celda.
 *  la cadena final debe tener la misma cantidad de caracteres que el número de celdas declaradas:
 *    * n para usar NBS en el paso del algoritmo definido para el tamaño de celda correspondiente
 *    * s para usar Screening en el paso del algoritmo definido para el tamaño de celda correspondiente
 *    * a para determinar de forma Automática el algoritmo para el tamaño de celda correspondiente
 *
 * Cuando se ejecuta esta clase actualiza los contactos encontrados en el contenedor de itneracciones de la Simulacion
 */
class MGCDCollisionDetector: public Engine {
public:

	MGCDCollisionDetector();

    /**
     * Aquí se efectúa la búsqueda de colisiones
     */
	void execute(Simulation* sim);
	virtual ~MGCDCollisionDetector();

	string getAlgConf() const {
		return algConf;
	}

	void setAlgConf(string algConf) {
		this->algConf = algConf;
	}

	struct AABB3D* getElems() const {
		return elems;
	}

	void setElems(struct AABB3D* elems) {
		this->elems = elems;
	}

	void setGrids(Float* grids, int gridsCount) {
		this->gridsCount = gridsCount;
		for(int i=0;i<gridsCount;i++) this->grids[i] = grids[i];
	}

	int getGridsCount() const {
		return gridsCount;
	}

private:
    ///Lista de tamaños de celdas a utilizar para cada grilla
	Float grids[100];
    ///Cantidad de grillas a utilizar
	int gridsCount;
    ///Configuración de los algoritmos a utlizar en cada paso
	string algConf;

    ///Lista de las cajas acotadoras de las partículas
	struct AABB3D * elems;

    /**
     * Esta función actualiza la lista de cajas acotadoras de las partículas
     */
	void updateBoundingBoxs(Simulation*& sim);
};

#endif /* MGCDCOLLISIONDETECTOR_H_ */
