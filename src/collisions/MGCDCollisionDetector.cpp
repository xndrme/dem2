/*
 * MGCDCollisionDetector.cpp
 *
 *  Created on: abr 5, 2014
 *      Author: Alvaro Javier
 */

#include "MGCDCollisionDetector.h"
#include "boost/shared_ptr.hpp"
#include <core/AABB.h>
#include <cstdio>

using namespace boost;


//FILE *out_file;
MGCDCollisionDetector::MGCDCollisionDetector() : gridsCount(0), algConf(""), elems(NULL) {}

void handler(Int i, Int j, struct AABB3D *elems, void *context) {
	Simulation* sim = (Simulation*) context;
	InteractionsContainer& ic = sim->getInteractions();
	if ( sim->getParticles()[i]->isMobile() || sim->getParticles()[j]->isMobile()) {
		ic.insert(i, j);
	}
}

void MGCDCollisionDetector::updateBoundingBoxs(Simulation*& sim) {
	for (unsigned int i = 0; i < sim->getParticles().size(); i++) {
		shared_ptr < Particle > p = sim->getParticles()[i];
		VectorND c = p->getCenter(); //obtenter la posicion de la particula
		AABB box = p->getShape()->getAABB(c);//calcular el bounding box
		c = (box.getMax() + box.getMin()) * 0.5; //para los contactos el centro es el centro del bbox
		elems[i].x = c.x();
		elems[i].y = c.y();
		elems[i].z = c.z();
		VectorND rad = (box.getMax() - box.getMin()) * 0.5;//radios 1/2 del bbox
		elems[i].rx = rad.x();
		elems[i].ry = rad.y();
		elems[i].rz = rad.z();
	}
}

void MGCDCollisionDetector::execute(Simulation* sim) {

	if(sim->getIter() == 0) {
		elems = (AABB3D*) malloc(sim->getParticles().size()*sizeof(AABB3D));
	}

	updateBoundingBoxs(sim);
	int res = mgcdContactDetect3D(grids, gridsCount, elems, sim->getParticles().size(), algConf.c_str(), &handler, sim);

	switch(res) {
	case MGCD_METHOD_ERROR:
		printf("Invalid methods configuration\n");
		break;
	case MGCD_GRID_ERROR:
		printf("Invalid grid configuration, eg. invalid or duplicate cell size or can not find a grid to fit a box in\n");
		break;
	case MGCD_BOX_ERROR:
		printf("invalid box information, eg. negative size of a box\n");
		break;
	default:
		break;
	}


}

MGCDCollisionDetector::~MGCDCollisionDetector() {
	free(elems);
//	fclose(out_file);
}

