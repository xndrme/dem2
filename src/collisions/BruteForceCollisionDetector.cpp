/* 
 * File:   BruteForceCollisionDetector.cpp
 * Author: soad
 * 
 * Created on June 18, 2011, 12:27 PM
 */

#include "BruteForceCollisionDetector.h"
#include <iostream>
#include <complex>

using boost::shared_ptr;
using namespace std;

void BruteForceCollisionDetector::execute(Simulation *sim) {
	//iterar por las interacciones apra detectar las colisiones
	unsigned int size = sim->getParticles().size();
	for (unsigned int i = 0; i < size - 1; i++) {
		for (unsigned int k = i + 1; k < size; k++) {
			//primero revisar si esta interacción ya está
			if ((bool) sim->getInteractions().find(i, k))
				continue;
			if ((sim->getParticles()[i]->isMobile()) || (sim->getParticles()[k]->isMobile()) ) {
				 sim->getInteractions().insert(i, k);
				//cout << "{" << i + 1 << "," << k + 1 << "}\n";
			}
		}
	}
}


BruteForceCollisionDetector::~BruteForceCollisionDetector() {
}

