/*
 * File:   BruteForceCollinder.h
 * Author: soad
 *
 * Created on June 18, 2011, 12:27 PM
 */

#ifndef BRUTEFORCECOLLISIONDETECTOR_H
#define	BRUTEFORCECOLLISIONDETECTOR_H

#include <core/Engine.h>

/**
 * @brief Busca las interacciones entre las particulas una a una sin optimizacion. Útil para debuggear.
 */
class BruteForceCollisionDetector: public Engine {
public:
	virtual void execute(Simulation *sim);
	virtual ~BruteForceCollisionDetector();
};

#endif	/* BRUTEFORCECOLLISIONDETECTOR_H */

