/*
 * GeomTriangleSphere.h
 *
 *  Created on: 15/04/2015
 *      Author: xndrme
 */

#ifndef CONTACTS_GEOMTRIANGLESPHERE_H_
#define CONTACTS_GEOMTRIANGLESPHERE_H_

#include <core/Particle.h>
#include <core/ContactGeometry.h>
#include <core/Dispatcher.h>

/**
 * @brief Maneja la interacción Triángulo-Esfera.
 *
 * El algoritmo implementado es una ligera modificación de este:
 * http://www.geometrictools.com/Documentation/IntersectionMovingSphereTriangle.pdf
 */
class GeomTriangleSphere: public GeometryFunctor {

public:
    /**
     * Aquí se calcula la interacción geométrica entre dos partículas y se devuelve una geometría de contacto por medio
     * del parámetro geom.
     *
     * @param p1: puntero a una partícula cuya Forma es un Triángulo
     * @param p2: puntero a una partícula cuya forma es una Esfera
     * @param geom: geometría de contacto, se actualiza si ya existe o se crea en caso de que no.
     */
    virtual bool go(const boost::shared_ptr<Particle>& p1, const boost::shared_ptr<Particle>& p2, boost::shared_ptr<ContactGeometry>& geom);

	GeomTriangleSphere() : E0(Vector2D::Zero()),E1(Vector2D::Zero()),E2(Vector2D::Zero()),Q0(Vector2D::Zero()),Q1(Vector2D::Zero()),Q2(Vector2D::Zero()){}
	virtual ~GeomTriangleSphere();

	int getPosition(Vector2D Y);

private:
	Vector2D E0,E1,E2;
	Vector2D Q0,Q1,Q2;

};

#endif /* CONTACTS_GEOMTRIANGLESPHERE_H_ */
