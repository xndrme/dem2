/*
 * File:   GeomSphereSphere.h
 * Author: soad
 *
 * Created on July 2, 2011, 12:01 PM
 */

#ifndef GEOMSPHERESPHERE_H
#define	GEOMSPHERESPHERE_H

#include <core/Particle.h>
#include <core/ContactGeometry.h>
#include <core/Dispatcher.h>

/**
 * @brief Maneja la interacción Esfera-Esfera
 */
class GeomSphereSphere: public GeometryFunctor {
public:
    /**
     * Aquí se calcula la interacción geométrica entre dos partículas y se devuelve una geometría de contacto por medio
     * del parámetro geom
     *
     * @param p1: puntero a una partícula
     * @param p2: puntero a una partícula
     * @param geom: geometría de contacto, se actualiza si ya existe o se crea en caso de que no.
     */
	virtual bool go(const boost::shared_ptr<Particle>& p1, const boost::shared_ptr<Particle>& p2, boost::shared_ptr<ContactGeometry>& geom);

    virtual ~GeomSphereSphere();

};

#endif	/* GEOMSPHERESPHERE_H */

