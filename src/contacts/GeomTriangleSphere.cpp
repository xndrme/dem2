/*
 * GeomTriangleSphere.cpp
 *
 *  Created on: 15/04/2015
 *      Author: xndrme
 */

#include "GeomTriangleSphere.h"
#include <shapes/Sphere.h>
#include <shapes/Triangle.h>
#include <core/MathDefs.h>

using boost::shared_ptr;

bool GeomTriangleSphere::go(const boost::shared_ptr<Particle>& p1, const boost::shared_ptr<Particle>& p2, boost::shared_ptr<ContactGeometry>& geom) {
	Triangle* triangle = static_cast<Triangle*>(p1->getShape().get());
	Sphere* sphere = static_cast<Sphere*>(p2->getShape().get());

	VectorND triangleCenter = p1->getCenter();

	VectorND C = p2->getCenter(); //centro de la esfera
	Real R = sphere->getRadius(); //radio

	//punts del triángulo, como sobre la partícula lo que tenenmos es el centro
	//debemos calcularlos usando los vectores de posición con respecto al centro
	VectorND P0(triangle->getV0()+triangleCenter);
	VectorND P1(triangle->getV1()+triangleCenter);
	VectorND P2(triangle->getV2()+triangleCenter);

	VectorND S1 = P1-P0, S2 = P2-P0; //aristas del triángulo

	VectorND U2 = S1.cross(S2).normalized(); //normal del plano del triángulo
	VectorND U0 = S1.normalized(); // como eje X para el plano del triángulo se toma la primera arista
	VectorND U1 = U2.cross(U0); // el eje Y queda definido por el eje X y la normal del plano

	//P0 es (0,0) en el plano, denotado por Q0
	Q1 = Vector2D(S1.norm(),0); // P1 en el plano
	Q2 = Vector2D(U0.dot(S2),U1.dot(S2)); //P2 en el plano

	//vectores en 2D que representan las aristas del triángulo
	E0 = Q1-Q0, E1 = Q2-Q1, E2 = Q0-Q2;

	//componentes del centro de la esfera según el eje de coordenadas
	//definido por el plano sobre P0
	Real y0 = U0.dot(C-P0), y1 = U1.dot(C-P0);//, y2 = U2.dot(C-P0);

	//Proyección del centro de la esfera en el plano, en coordenadas planares
	Vector2D Y(y0,y1);

	int yPos = getPosition(Y);//posicion del punto con respecto a los lados del triángulo
	VectorND nearPoint = VectorND::Zero(); //punto del triángulo mas cercano al centro
	Real s;//escalar que define la posicion del punto mas cercano sobre el lado
	Real handy;//utilitario para cálculos
	switch(yPos) {
		case 0:
			//dentro del triángulo, el punto más cercano es la proyección
			nearPoint = P0 + y0*U0 + y1*U1;
			break;
		case 1:
			//en la región correspondiente al lado P0P1
			handy = E0.norm();
			s = E0.dot(Y-Q0)/handy/handy;
			if(s<=0) nearPoint = P0;
			else if (s>=1) nearPoint = P1;
			else nearPoint = P0 + s*(P1-P0);
			break;
		case 2:
			//en la región correspondiente al lado P1P2
			handy = E1.norm();
			s = E1.dot(Y-Q1)/handy/handy;
			if(s<=0) nearPoint = P1;
			else if (s>=1) nearPoint = P2;
			else nearPoint = P1 + s*(P2-P1);
			break;
		case 3:
			//región del punto P1
			nearPoint = P1;
			break;
		case 4:
			//en la región correspondiente al lado P2P0
			handy = E2.norm();
			s = E2.dot(Y-Q2)/handy/handy;
			if(s<=0) nearPoint = P2;
			else if (s>=1) nearPoint = P0;
			else nearPoint = P2 + s*(P0-P2);
			break;
		case 5:
			//región del punto P0
			nearPoint = P0;
			break;
		case 6:
			//región del punto P2
			nearPoint = P2;
			break;
	}

	//distancia desde la esfera al triángulo
	Real dist = (C-nearPoint).norm() - R;

	bool isReal = (bool) geom; //para saber si es real la geometría del contacto o si hay que hacerla nueva
	if (!isReal) geom = shared_ptr<ContactGeometry>(new ContactGeometry);

	//Penetración
	Real urn = -dist;
	//si la interacción no era real anteriormente entonces se están acercando
	if(!isReal) {
		geom->setApproaching(true);
	} else {
		//verificar con la penetración anterior para saber si se acercan o se alejan
		geom->setApproaching(geom->getURN() < urn);
	}
	//actualizar la penetración
	geom->setURN(urn);

	VectorND normal = (C-nearPoint).normalized();
	if(yPos==0) {
		normal = MathDefs::sign(U2.dot(C-nearPoint))*U2;
	}
	geom->setNormal(normal);
	geom->setContactPoint(nearPoint);

	return true;
}

GeomTriangleSphere::~GeomTriangleSphere() {
}

int GeomTriangleSphere::getPosition(Vector2D Y) {
	int p = 0;

	//en p se guarda codificado en binario si esta a la derecha del segmento el
	//punto Y, para cada uno de los tres segmentos del triángulo
	//los calculos se realizan suponiendo esta posicion PiYPi+1
	p += (Y.x()-Q0.x())*(Q1.y()-Q0.y()) - (Q1.x()-Q0.x())*(Y.y()-Q0.y()) > 0 ? 1 : 0;
	p += (Y.x()-Q1.x())*(Q2.y()-Q1.y()) - (Q2.x()-Q1.x())*(Y.y()-Q1.y()) > 0 ? 2 : 0;
	p += (Y.x()-Q2.x())*(Q0.y()-Q2.y()) - (Q0.x()-Q2.x())*(Y.y()-Q2.y()) > 0 ? 4 : 0;

	return p;
}
