/* 
 * File:   GeomSphereSphere.cpp
 * Author: soad
 * 
 * Created on July 2, 2011, 12:01 PM
 */

#include "GeomSphereSphere.h"
#include <core/MathDefs.h>
#include <shapes/Sphere.h>

using boost::shared_ptr;

bool GeomSphereSphere::go(const shared_ptr<Particle>& p1, const shared_ptr<Particle>& p2, shared_ptr<ContactGeometry>& geom) {
	Sphere* p1shpere = static_cast<Sphere*>(p1->getShape().get());
	Sphere* p2sphere = static_cast<Sphere*>(p2->getShape().get());

	//distancia entre los centros de las esferas
	Real dist = (p1->getCenter() - p2->getCenter()).norm();
	//suma de los radios
	Real radiiSum = p1shpere->getRadius() + p2sphere->getRadius();


	bool isReal = (bool) geom; //para saber si es real la geometría del contacto o si hay que hacerla nueva
	if (!isReal) geom = shared_ptr<ContactGeometry>(new ContactGeometry);

	//distancia entre los centros de las esferas anteriormente
	Real distBefore = (p1->getCenterBefore() - p2->getCenterBefore()).norm();
	geom->setApproaching((distBefore - dist) > 0);

	//Penetración
	Real urn = radiiSum - dist;
	geom->setURN(urn);

	VectorND normal = (p2->getCenter() - p1->getCenter()).normalized();
	geom->setNormal(normal);
	geom->setContactPoint(0.5 * (p1->getCenter() + p2->getCenter() + (p1shpere->getRadius() - p2sphere->getRadius()) * normal));

	return true;

}

GeomSphereSphere::~GeomSphereSphere() {
}

