/* 
 * File:   GeomSpherePlane.cpp
 * Author: soad
 * 
 * Created on July 2, 2011, 12:16 PM
 */

#include "GeomSpherePlane.h"
#include <shapes/Sphere.h>
#include <shapes/Plane.h>
#include <iostream>
#include <core/MathDefs.h>

using boost::shared_ptr;
using namespace std;

bool GeomSpherePlane::go(const shared_ptr<Particle>& p1, const shared_ptr<Particle>& p2, shared_ptr<ContactGeometry>& geom) {
	Sphere* sphere = static_cast<Sphere*>(p1->getShape().get());
	Plane* plane = static_cast<Plane*>(p2->getShape().get());

	VectorND vecFromPlane = p2->getCenter() - p1->getCenter();
	Real distPointPlane = abs(vecFromPlane.dot(plane->getNormal()));

	bool isReal = (bool) geom;//para saber si ya existe una geometría de contactos

	//en caso de que no sea real todavia la geometría del contacto
	//si la esfera corte al plano entonces crear la interacción
	if (!isReal && (distPointPlane < sphere->getRadius()))
		geom = shared_ptr<ContactGeometry>(new ContactGeometry);
	else //en este caso no hay interacción
		return false;

	Real distPointPlaneBefore = abs((p2->getCenterBefore() - p1->getCenterBefore()).dot(plane->getNormal()));
	geom->setApproaching(distPointPlane	< distPointPlaneBefore);

	//esta normal además de servir de normal al plano tiene el sentido hacia la esfera
	//esto es importante porque la esfera puede estar en cualquier lado del plano
	VectorND normal = MathDefs::sign(plane->getNormal().dot(vecFromPlane)) * plane->getNormal();
	geom->setNormal(normal);
	//punto de contacto en la esfera: sp = c2-n*R
	//punto de contacto en el plano: pp = c2-(n.v)*n
	//punto de contacto representativo: (sp+pp)/2 = c2 - 0.5*(R+n.v)*n
	geom->setContactPoint(p2->getCenter() - 0.5*(sphere->getRadius() + normal.dot(vecFromPlane))*normal);
	geom->setURN(sphere->getRadius() - distPointPlane);

	return true;

}

GeomSpherePlane::~GeomSpherePlane() {
}

