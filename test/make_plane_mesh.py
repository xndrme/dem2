# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
import numpy.linalg as la
import vtk
#import visual as vp


def get_triangles(P,A0,A1,L,n,m):
    A0 /= la.norm(A0)
    A1 /= la.norm(A1)
    
    centers = []
    for i in range(0,n):
        for j in range(0,m):
            p0 = P + A0*L*i + A1*L*j
            p1 = P + A0*L*(i+1) + A1*L*j
            p2 = P + A0*L*(i+1) + A1*L*(j+1)
            p3 = P + A0*L*i + A1*L*(j+1)
            c = p0 + p1 + p3
            c /= 3.0
            centers.append(c)
            c = p1 + p3 + p2
            c /= 3.0
            centers.append(c)
    
    c = centers[0]
    v0 = P - c
    v1 = P + A0*L - c
    v2 = P + A1*L - c
    
    c = centers[1]
    v3 = P + A0*L - c
    v4 = P + A0*L + A1*L - c
    v5 = P + A1*L - c

    tr = []
    for i,c in enumerate(centers):
        if i%2==0:
            tr.append([c,v0,v1,v2])
        else:
            tr.append([c,v3,v4,v5])
    
    return np.array(tr)
    
def format_output(tr,file_name=None):
    import sys
    f = sys.stdout    
    if file_name:
        f = open(file_name,"wt")    
    s = ",".join((["%f"]*12)) + ",0"
    for d in tr:
        print(s%tuple(d.reshape(-1)),file=f)

def make_vtk_grid(tr,fname):
    cs  = tr[:,0]

    p0s = cs + tr[:,1]
    p1s = cs + tr[:,2]
    p2s = cs + tr[:,3]
    
    ps = vtk.vtkPoints()    
    ids = vtk.vtkIdList()        
    for i,p0,p1,p2 in zip(xrange(len(p0s)),p0s,p1s,p2s):
        ps.InsertNextPoint(*p0)        
        ps.InsertNextPoint(*p1)
        ps.InsertNextPoint(*p2)
        ids.InsertNextId(3*i)
        ids.InsertNextId(3*i+1)
        ids.InsertNextId(3*i+2)        

    triGrid = vtk.vtkUnstructuredGrid()
    triGrid.SetPoints(ps)        
    for i in xrange(len(p0s)):
        tri = vtk.vtkTriangle()
        tri.GetPointIds().SetId(0,3*i)
        tri.GetPointIds().SetId(1,3*i+1)
        tri.GetPointIds().SetId(2,3*i+2)
        triGrid.InsertNextCell(tri.GetCellType(),tri.GetPointIds())

    w = vtk.vtkXMLUnstructuredGridWriter()
    w.SetFileName(fname)
    w.SetInput(triGrid)
    w.Write()    

tr = get_triangles([-12,-12,-1.5],[0,1,0],[1,0,-1],1.41,16,16)
format_output(tr,"D:/tri.csv")
make_vtk_grid(tr,"D:/tri.vtu")


#ms = np.sum(tr,axis=1)/3.0
#
#tr = tr.reshape(-1,3)
#cl = [[np.random.rand(),np.random.rand(),np.random.rand()] for t in tr]
#
#f = vp.frame()
#tri = vp.faces(
#    pos = tr,
#    color=cl,
#    frame = f
#)
#tri.make_normals()
#tri.make_twosided()
#
#n = vp.points(pos=ms)




