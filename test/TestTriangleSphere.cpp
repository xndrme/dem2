/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include <vector>

using namespace boost;


TEST(TestTriangleSphere, testRunPlane) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testTriangleSphere.txt");
	Simulation *sim = csv_loader.read();
	sim->run();
}

TEST(TestTriangleSphere, testRunVertex) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testTriangleSphere_Vertex.txt");
	Simulation *sim = csv_loader.read();
	sim->run();
}

TEST(TestTriangleSphere, testRunEdge) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testTriangleSphere_Edge.txt");
	Simulation *sim = csv_loader.read();
	sim->run();
}

TEST(TestTriangleSphere, testRunMultiple) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testTriangleSphere_Multiple.txt");
	Simulation *sim = csv_loader.read();
	sim->run();
}

TEST(TestTriangleSphere, testRunMeshPlane) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testTriangleSphere_Plane.txt");
	Simulation *sim = csv_loader.read();
	sim->run();
}





