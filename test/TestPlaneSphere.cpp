/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include <vector>

using namespace boost;


TEST(TestPlaneSphere, testRun) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testPlaneSphere.txt");
	Simulation *sim = csv_loader.read();

	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new TxtRecorder("test_output/dump_PlaneSphere.txt", 500)));
	sim->run();
}




