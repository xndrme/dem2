/*
 * main.cpp
 *
 *  Created on: Oct 16, 2013
 *      Author: alvarojavier
 */

#include <gtest/gtest.h>

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


