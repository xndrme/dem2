/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include <vector>

using namespace boost;


TEST(TestCube, Compression) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/cube_compression.txt");
	Simulation *sim = csv_loader.read();
	sim->run();
}




