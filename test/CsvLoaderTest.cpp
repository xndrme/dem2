/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"

using namespace boost;


TEST(CsvLoaderTest, testRead) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testCsvLoader.txt");
	Simulation *sim = csv_loader.read();
	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new TxtRecorder("test_output/dump_testCohesive6.txt", 100)));
	sim->run();
}




