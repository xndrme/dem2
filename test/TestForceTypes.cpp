/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include <vector>

using namespace boost;


TEST(TestForceTypes, testConstantForce) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testForceTypes.txt");
	Simulation *sim = csv_loader.read();
	sim->run();
}




