/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "core/Particle.h"
#include "forces/ConstantVelocity.h"

#include <vector>

#include <boost/shared_ptr.hpp>

using boost::shared_ptr;

TEST(TestConcepts, testRun) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testPartsConcepts.txt");
	Simulation *sim = csv_loader.read();


	vector<int> toParts;
	toParts.push_back(6);
	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new ConstantVelocity(VectorND(0,0,-5), toParts)));

	sim->run();
}





