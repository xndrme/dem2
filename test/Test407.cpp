/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include <vector>

using namespace boost;


TEST(Test407Parts, testRun) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/test407Parts.txt");
	Simulation *sim = csv_loader.read();

//	int upper[69] = {294,295,314,316,318,321,322,327,331,332,333,334,335,336,338,339,340,343,347,348,349,352,353,354,355,356,357,359,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,388,389,390,393,394,395,396,397,398,399,401,402,403,404,405,406};
//	vector<int> toParts(69);
//	for(int i=0;i<69;i++)
//		toParts[i] = upper[i];
//
//	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new ConstantForce(VectorND(0,0,-20000000), toParts)));

	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new TxtRecorder("test_output/dump_test407.txt", 500)));


	sim->run();
}




