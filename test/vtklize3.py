import numpy as np
import csv
import argparse
import vtk
import os

parser = argparse.ArgumentParser()
parser.add_argument("DEMdumpFile", type=argparse.FileType('rt'), help="output file from TxtRecorder")
args = parser.parse_args()

filePath = args.DEMdumpFile.name
pointPos = filePath.rfind(".")
if pointPos!=-1:
    filePath = filePath[:pointPos]
dirName = filePath + ".vtkFiles"
if not os.path.exists(dirName):
    os.mkdir(dirName)

reader = csv.reader(args.DEMdumpFile,delimiter=' ',skipinitialspace=True)


headers = reader.next()
iter_idx = headers.index('iter')
rad_idx = headers.index('rad')
cx_idx = headers.index('cx')
last_iter = -1
data = {}
num = 0

w = vtk.vtkXMLUnstructuredGridWriter()
spheres_holder = []
af = vtk.vtkAppendFilter()

for row in reader:
    if row[iter_idx]==last_iter:
        s = vtk.vtkSphereSource()
        s.SetRadius(float(row[rad_idx]))
        s.SetCenter(*map(float,row[cx_idx:cx_idx+3]))
        af.AddInput(s.GetOutput())
        spheres_holder.append(s)        
    elif last_iter!=-1:
        af.Update()
        w.SetInput(af.GetOutput())
        w.SetFileName("%s/out%03i.vtu"% (dirName,num))
        w.Write()
        num += 1
        
        af = vtk.vtkAppendFilter()
        w.SetInput(af.GetOutput())
        spheres_holder = []
    last_iter = row[iter_idx]        
        


f2 = open(args.DEMdumpFile.name.replace(".txt","_triangles.txt"),"rt")
reader = csv.reader(f2,delimiter=' ',skipinitialspace=True)

headers = reader.next()


iter_idx = headers.index('iter')
v0x_idx = headers.index('v0x')
v1x_idx = headers.index('v1x')
v2x_idx = headers.index('v2x')
cx_idx = headers.index('cx')
last_iter = -1
data = {}
num = 0


af = vtk.vtkAppendFilter()
ps = vtk.vtkPoints()    
ids = vtk.vtkIdList()
i = 0
for row in reader:
    if row[iter_idx]==last_iter:
        cs  = np.array(map(float,row[cx_idx:cx_idx+3]))
        
        p0 = cs + np.array(row[v0x_idx:v0x_idx+3],dtype=float)
        p1 = cs + np.array(row[v1x_idx:v1x_idx+3],dtype=float)
        p2 = cs + np.array(row[v2x_idx:v2x_idx+3],dtype=float)
        
        ps.InsertNextPoint(*p0)
        ps.InsertNextPoint(*p1)
        ps.InsertNextPoint(*p2)
        ids.InsertNextId(3*i)
        ids.InsertNextId(3*i+1)
        ids.InsertNextId(3*i+2)        
        i+=1
              
    elif last_iter!=-1:
        triangles_holder = []
        triGrid = vtk.vtkUnstructuredGrid()
        triGrid.SetPoints(ps)        
        for j in xrange(i):
            tri = vtk.vtkTriangle()
            triangles_holder.append(tri)
            tri.GetPointIds().SetId(0,3*j)
            tri.GetPointIds().SetId(1,3*j+1)
            tri.GetPointIds().SetId(2,3*j+2)
            triGrid.InsertNextCell(tri.GetCellType(),tri.GetPointIds())
        
        w.SetFileName("%s/triangles%03i.vtu"% (dirName,num))
        w.SetInput(triGrid)
        w.Write()
        num += 1
        
        ps = vtk.vtkPoints()    
        ids = vtk.vtkIdList()
        i = 0

    last_iter = row[iter_idx]        

#
#num = 0
#for _,g in groups:
#
#    cs  = np.array(g[['cx','cy','cz']].values)
#
#    p0s = cs + np.array(g[['v0x','v0y','v0z']].values)
#    p1s = cs + np.array(g[['v1x','v1y','v1z']].values)
#    p2s = cs + np.array(g[['v2x','v2y','v2z']].values)
#    
#    ps = vtk.vtkPoints()    
#    ids = vtk.vtkIdList()        
#    for i,p0,p1,p2 in zip(xrange(len(p0s)),p0s,p1s,p2s):
#        ps.InsertNextPoint(*p0)        
#        ps.InsertNextPoint(*p1)
#        ps.InsertNextPoint(*p2)
#        ids.InsertNextId(3*i)
#        ids.InsertNextId(3*i+1)
#        ids.InsertNextId(3*i+2)        
#
#    triGrid = vtk.vtkUnstructuredGrid()
#    triGrid.SetPoints(ps)        
#    for i in xrange(len(p0s)):
#        tri = vtk.vtkTriangle()
#        tri.GetPointIds().SetId(0,3*i)
#        tri.GetPointIds().SetId(1,3*i+1)
#        tri.GetPointIds().SetId(2,3*i+2)
#        triGrid.InsertNextCell(tri.GetCellType(),tri.GetPointIds())
#
#    w.SetFileName("%s/triangles%03i.vtu"% (dirName,num))
#    w.SetInput(triGrid)
#    w.Write()    
#    
#    num += 1
