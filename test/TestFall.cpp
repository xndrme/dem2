/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include <vector>

using namespace boost;


TEST(TestFall, testBox) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testBox.txt");
	Simulation *sim = csv_loader.read();

	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new TxtRecorder("test_output/dump_Box.txt", 500)));


	sim->run();
}

TEST(TestFall, testFallOverPlane) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testFallOverPlane.txt");
	Simulation *sim = csv_loader.read();

	sim->run();
}

TEST(TestFall, testFallOverPlaneHertz) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testFallOverPlaneHertz.txt");
	Simulation *sim = csv_loader.read();

	sim->run();
}

TEST(TestFall, testFallOverPlaneDamping) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testFallOverPlaneDamping.txt");
	Simulation *sim = csv_loader.read();

	sim->run();
}

TEST(TestFall, testFallOverPlaneFrictional) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testFallOverPlaneFrictionalOverTime.txt");
	Simulation *sim = csv_loader.read();

	sim->run();
}




