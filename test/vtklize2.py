import pandas as pd
import numpy as np
import argparse
import vtk
import os

parser = argparse.ArgumentParser()
parser.add_argument("DEMdumpFile", type=argparse.FileType('rt'), help="output file from TxtRecorder")
args = parser.parse_args()

df = pd.read_csv(args.DEMdumpFile,delim_whitespace=True)

groups = df.groupby('iter')

filePath = args.DEMdumpFile.name
pointPos = filePath.rfind(".")
if pointPos!=-1:
    filePath = filePath[:pointPos]
dirName = filePath + ".vtkFiles"
if not os.path.exists(dirName):
    os.mkdir(dirName)


w = vtk.vtkXMLUnstructuredGridWriter()

num = 0
for _,g in groups:
    
    af = vtk.vtkAppendFilter()
    
    for i,d in enumerate(g[['rad','cx','cy','cz']].values):
        s = vtk.vtkSphereSource()
        s.SetRadius(d[0])
        s.SetCenter(*d[1:])
        af.AddInput(s.GetOutput())
    af.Update()
        
    w.SetFileName("%s/out%03i.vtu"% (dirName,num))
    w.SetInput(af.GetOutput())
    w.Write()
    
    num += 1


tr = pd.read_csv(args.DEMdumpFile.name.replace(".txt","_triangles.txt"),delim_whitespace=True)
groups = tr.groupby('iter')

num = 0
for _,g in groups:

    cs  = np.array(g[['cx','cy','cz']].values)

    p0s = cs + np.array(g[['v0x','v0y','v0z']].values)
    p1s = cs + np.array(g[['v1x','v1y','v1z']].values)
    p2s = cs + np.array(g[['v2x','v2y','v2z']].values)
    
    ps = vtk.vtkPoints()    
    ids = vtk.vtkIdList()        
    for i,p0,p1,p2 in zip(xrange(len(p0s)),p0s,p1s,p2s):
        ps.InsertNextPoint(*p0)        
        ps.InsertNextPoint(*p1)
        ps.InsertNextPoint(*p2)
        ids.InsertNextId(3*i)
        ids.InsertNextId(3*i+1)
        ids.InsertNextId(3*i+2)        

    triGrid = vtk.vtkUnstructuredGrid()
    triGrid.SetPoints(ps)        
    for i in xrange(len(p0s)):
        tri = vtk.vtkTriangle()
        tri.GetPointIds().SetId(0,3*i)
        tri.GetPointIds().SetId(1,3*i+1)
        tri.GetPointIds().SetId(2,3*i+2)
        triGrid.InsertNextCell(tri.GetCellType(),tri.GetPointIds())

    w.SetFileName("%s/triangles%03i.vtu"% (dirName,num))
    w.SetInput(triGrid)
    w.Write()    
    
    num += 1
