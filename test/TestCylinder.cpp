/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include "forces/ConstantVelocity.h"
#include <vector>

using namespace boost;


TEST(TestCylinder, testRun) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testCylinder.txt");
	Simulation *sim = csv_loader.read();

	vector<int> toParts;
	unsigned int i = 0;
	while(i<3844/2) {
		toParts.push_back(5941+2*i);
		i++;
	}

	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new ConstantVelocity(VectorND(0,0,-20), toParts)));

	sim->run();
}

TEST(TestCylinder, testCompression) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testCylinderCompression.txt");
	Simulation *sim = csv_loader.read();

	vector<int> toParts;
	for(unsigned int i = 406+512;i<406+512+512;i++) {
		toParts.push_back(i);
	}

	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new ConstantVelocity(VectorND(0,0,-10), toParts)));

	sim->run();
}

TEST(TestCylinder, testTraction) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testCylinderTraction.txt");
	Simulation *sim = csv_loader.read();

	vector<int> toParts;
	for(unsigned int i = 406+512;i<406+512+512;i++) {
		toParts.push_back(i);
	}

	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new ConstantVelocity(VectorND(0,0,10), toParts)));
	sim->run();
}




