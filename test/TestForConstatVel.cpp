/*
 * SimulationLoaderTest.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: alvarojavier
 */


#include <gtest/gtest.h>
#include "utils/CsvSimulationLoader.h"
#include "utils/TxtRecorder.h"
#include "forces/ConstantForce.h"
#include "forces/ConstantVelocity.h"
#include <vector>

using namespace boost;


TEST(TestForConstantVel, testRun) {
	CsvSimulationLoader csv_loader = CsvSimulationLoader("data/testForConstantVel.txt");
	Simulation *sim = csv_loader.read();

	vector<int> toParts;
	toParts.push_back(0);


	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new ConstantVelocity(VectorND(0,0,-1000), toParts)));

//	sim->getGeneralPurposeEngines().push_back(shared_ptr<GeneralPurposeEngine>(new TxtRecorder("test_output/dump_testForConstantVel.txt", 500)));


	sim->run();
}




