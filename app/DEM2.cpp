//
// Created by Alvaro Javier on 7/1/2015.
//

#include <stdio.h>
#include <core/Simulation.h>
#include <utils/CsvSimulationLoader.h>

int main(int argn, char** args) {
    if(argn != 2) {
        printf("Debe especificar un fichero de entrada, ejemplo: DEM2.exe input_file.txt\n");
    } else {
        CsvSimulationLoader csv_loader = CsvSimulationLoader(args[1]);
        Simulation *sim = csv_loader.read();
        sim->run();
    }
    return 0;
}